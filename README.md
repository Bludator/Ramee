# User's Manual
This program is web-browser extension which should automate repetitive tasks on web pages.

## Software requirements:
- Google Chrome to run
- Node.js with npm  to compile/bundle

## Compiling / bundling
1. Navigate to directory with `package.json`
2. Run `npm install`
3. Run `npm run dev` or `npm run build` (to produce minified version)
4. The ready-to-use files are located at `./dist` or `./release` respectively

## How to run it
1. Navigate to Chrome's extension page for example by going to `chrome://extensions/`
2. Turn on developer mode (Upper-left corner)
3. Load `./dist` or `./release` folder

## How to use it

### Popup
Accessed by clicking extension's icon right of the navigation bar of Chrome 

To record new record:
1. Select `new Recording...`
2. (Optional) Click `Options`
    - Select mode of recording by selecting either `Form` or `Capture` 
    for tracking form elements or monitoring events respectively
    - Select what events to track in `Capture` mode (You could reset the selection)
3. Click `Play` 
4. Record events in case of `Capture` mode, or check/uncheck any form element by clicking (red highlight) it is also possible to mark submit button, which will be clicked after all elements are filled on repeating (green highlight) in `Form` mode
5. Stop recording by clicking `Done` to save or `Cancel` to not save recording
 in native notification (e.g. windows notifications ... make sure it is on and it is not enabled quiet mode)
6. Name your recording.

To repeat:
1. Select recording you want to play
2. (Optional) Click `Options`
    - Set numer of repetitions of recording
    - Speed of the repeating
    - CSV table with data to fill in forms (see [Options page](#options-page) for more info)
    - CSS selector to element to download after every repeating.
3. Click `Play` 
4. (Optional) repeating could be stopped via notifications.

### Options page
Accessed by `Mouse Right` icon in chrome -> `Options`.

#### Editing record
On the left one could select saved record. On the right there will appear editor where you can edit raw data, thought there are a few shortcuts:
- Change location where is record saved by clicking `Synchronized?`
- Renaming of the record could be accomplished by double-clicking on title/name
For more information about structure of raw data see definition of [[Record]]

There is also `Get CSV template` buttons which get CSV-like table with one line of comma separated values in form of `{label}:({values})`, where `{label}` is label, name, tag name or something like this to get user something to know what element it is; and `{value}` is `string`, `bool`, `number` or `|`-separated list of valid values.

#### Chaining record
Click `Chain...` click on records in order in what it will be chained, then save it by clicking `Save chain`.

# Programmer’s Documentation
Extension is split into 6 parts, one, Options page, is standalone and works just above saved data; the rest are arranged as depicts the schema below:

![Architecture diagram](../images/diagram.png)

Communication between two parts (shown as a fat arrow) is conducted by a "protocol" defined by interfaces defined in `./_typeDefinitions/`.

## Options page
UI part of the extension, where user could view and edit saved records.

## Popup
Popup is main UI component to operate the extension.

## Background script
Holds state of each tab's recording or repeating, so it is maintained between redirects/navigations. Also it could this state save - saving recordings.

## Content script
Parses input from `In-Page script` and handle form elements. 

## In-Page script
Provides a way to monitor and recreate events. It is meant to be ran as soon as possible on page. As lightweight as possible.

### Injecting In-Page script
Although there is one quite straightforward method to insert script in page before any other script - adding content script with `"run_at": "document_start"` property in manifest, it have one main drawback, it is inserted in every page. So there are used two other ways to inject script into page:
1. Redirecting one (usually first) script tag's `src` to `data` URL with In-Page script placed just before original script code
2. Injecting via devtools (see [Devtools script](#devtools-script))

What method to use is decided in [[IsSafe]]

### Event monitoring & replicating
All event listeners are saved and wrapped inside function which is invoked when the events occures by changing `addEventListener` method of prototype of `EventTarget` and also changing objects with defined `on`- events. This function enables logging of events.

When event should be fired/replicated the saved listeners are directly invoked with event-like object .

## Devtools script
Devtools script is used just to enable devtool api - `chrome.devtools.inspectedWindow.reload` which provide a way to inject script into page before any other script already ran.


