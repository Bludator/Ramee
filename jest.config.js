module.exports = {
    automock: false,
    cacheDirectory: ".\\.jest_cache",
    clearMocks: true,
    collectCoverage: false,
    collectCoverageFrom: ["src/**/*.[jt]s"],
    coverageDirectory: ".\\coverage",
    coveragePathIgnorePatterns: [".*/_typeDefinitions/.*", ".*[tT]ypes.ts", ".*/I[A-Z].*.ts"],
    globals: {
        "ts-jest": {
            diagnostics: {
                warnOnly: true,
            },
        },
    },
    preset: "ts-jest",
    //Important to properly test side effect
    resetModules: true,
    setupFiles: ["jest-webextension-mock", "./tests/testSetup/parcel.js"],
    setupFilesAfterEnv: ["jest-extended"],
    testMatch: ["**/tests/**/*.[jt]s", "**/*.+(spec|test).[jt]s"],
    testPathIgnorePatterns: ["/testHelpers/"],
};
