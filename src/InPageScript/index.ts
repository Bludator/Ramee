import finder from "@medv/finder";
import { InfoError, toCS, toMS } from "../_common/typeDefinitions/InPageScriptMessaging";
import { EventCtors, EventData } from "../_common/typeDefinitions/record";
import onEvents from "./onEvents";

/**
 * Wrapper of `Error` constructor, posts message to content script
 */
class PostedError extends Error {
    constructor(error: InfoError, errorMsg?: string) {
        super(errorMsg);
        port.postMessage({ msgType: toCS.msgType.error, error: { error, errorMsg } } as toCS.ErrorMsg);
    }
}
// tslint:disable-next-line: interface-name
/**
 * Represents definition/registration of event listener
 */
interface EventDef {
    type: string;
    listener: EventListener | EventListenerObject | undefined;
    options?: AddEventListenerOptions;
    isOnListener: boolean;
}
/**
 * ID of this extension
 */
declare let id: string;

/**
 * Symbolic name for event's ID
 */
const sID = Symbol("id");
type EventWithID = Event & { [sID]?: number };
/**
 * If set all events listed in [[events]] will be sent to content script
 */
let isRecording = false;
/**
 * list of all registered listeners by event target
 */
const listeners = new Map<EventTarget, EventDef[]>();
/**
 * List of captured event types
 */
let events: Set<string>;
const channel = new MessageChannel();
/**
 * port to content script
 */
const port = channel.port1;
window.dispatchEvent(new MessageEvent(id, { data: "", ports: [channel.port2] }));
port.onmessage = a => {
    const msg: toMS.Msg = (a as MessageEvent).data;
    switch (msg.cmd) {
        case toMS.command.recEvents:
            events = new Set((msg as toMS.RecEventMsg).events);
            isRecording = true;
            break;
        case toMS.command.stop:
            isRecording = false;
            break;
        case toMS.command.fireEvent:
            fireEvent((msg as toMS.FireEventMsg).data);
            break;
        default:
            throw new PostedError(InfoError.Unexpected, "Invalid incoming message" + JSON.stringify(msg));
    }
};

/**
 * Original `EventTarget.prototype.addEventListener`
 */
const originalAddEventListener = EventTarget.prototype.addEventListener;
/**
 * last event number/ID
 */
let eventID = 1;

// todo? removeEventListener
EventTarget.prototype.addEventListener = function(
    this: EventTarget,
    type: string,
    listener: EventListener | EventListenerObject | null,
    options?: boolean | AddEventListenerOptions
): void {
    // tslint:disable-next-line: no-null-keyword
    if (listener == null) {
        // what if....
        originalAddEventListener.call(this, type, listener, options);
        return;
    }
    saveListener(this || window, type, listener, options);
    originalAddEventListener.call(
        this,
        type,
        (event: EventWithID) => {
            if (isRecording) {
                if (event[sID] === undefined) {
                    event[sID] = eventID++;
                }
                saveEvent(event);
            }
            try {
                if (typeof listener === "function") {
                    listener.call(this || window, event);
                } else {
                    listener.handleEvent(event);
                }
            } catch (error) {
                console.error(error);
            }
        },
        options
    );
};
for (const event of onEvents) {
    wrapOnEvent(event, HTMLElement.prototype);
    wrapOnEvent(event, window);
    wrapOnEvent(event, document);
}

/**
 * Alter behavior of on-event properties, so it will log event listeners and events
 * @param type event type
 * @param object object of which the properties will be wrapped
 */
function wrapOnEvent(type: string, object: GlobalEventHandlers) {
    const originalDescriptor = Object.getOwnPropertyDescriptor(object, type);
    if (originalDescriptor) {
        Object.defineProperty(object, type, {
            get() {
                if (!listeners.has(this || window)) {
                    return undefined;
                }
                const targetsListeners = listeners.get(this || window) as EventDef[];
                for (const listenerDef of targetsListeners) {
                    if (listenerDef.isOnListener && listenerDef.type === type.substring(2)) {
                        return listenerDef.listener;
                    }
                }
                return undefined;
            },
            set(value) {
                saveListener(this || window, type.substring(2), value, { capture: false }, true);
                if (typeof value === "function") {
                    originalDescriptor.set!.call(this || window, function(this: any, event: EventWithID) {
                        if (isRecording) {
                            if (event[sID] === undefined) {
                                event[sID] = eventID++;
                            }
                            saveEvent(event);
                        }
                        try {
                            value.call(this, event);
                        } catch (error) {
                            console.error(error);
                        }
                    });
                } else {
                    originalDescriptor.set!.call(this || window, value);
                }
            },
        });
    }
}
/**
 * Saves event listener as [[EventDef]] into [[events]]
 * @param target target on which the listener listens
 * @param type type of the event
 * @param listener supplied listeners
 * @param options any options given in `addEventListener` function
 * @param isOnListener if it is listeners added by on- property
 */
function saveListener(
    target: EventTarget,
    type: string,
    listener: EventListener | EventListenerObject,
    options?: boolean | AddEventListenerOptions,
    isOnListener = false
) {
    if (listeners.has(target)) {
        const targetsListeners = listeners.get(target) as EventDef[];
        if (isOnListener) {
            for (const listenerDef of targetsListeners) {
                if (listenerDef.isOnListener && listenerDef.type === type) {
                    listenerDef.listener = listener;
                    return;
                }
            }
        }
        targetsListeners.push({
            listener,
            options: typeof options === "boolean" ? { capture: options } : options,
            type,
            isOnListener,
        });
        listeners.set(target, targetsListeners);
    } else {
        listeners.set(target, [
            { listener, options: typeof options === "boolean" ? { capture: options } : options, type, isOnListener },
        ]);
    }
}

/**
 * Saves events listed in [[events]]
 * @param event event to save
 */
function saveEvent(event: Event) {
    if (!events.has(event.type)) {
        return;
    }
    // filter out my message events
    const options = {} as { [a: string]: string | boolean | number };

    // tslint:disable-next-line: forin
    for (const key in event) {
        const property = (event as any)[key];
        if (typeof property === "string" || typeof property === "boolean" || typeof property === "number") {
            options[key] = property;
        }
    }
    port.postMessage({
        msgType: toCS.msgType.eventData,
        id: (event as any)[sID],
        type: event.type,
        ctor: event.constructor.name as EventCtors,
        targetQuery: serializeEventTarget(event.target),
        currentTarget: serializeEventTarget(event.currentTarget),
        options,
    } as toCS.EventDataMsg);
}
/**
 * Finds CSS selector or returns `<window` `<document` for window and document respectively
 * @param target Event target to serialize
 * @returns
 */
function serializeEventTarget(target: EventTarget | null): string {
    if (target === window) return "<window";
    if (target === document) return "<document";
    if (target instanceof Element) return finder(target);
    throw new PostedError(InfoError.Selector_CanNotCreate, `Can not create CSSSelector for: "${target}"`);
}
/**
 * Deserialize strings serialized by [[serializeEventTarget]]
 * @param target string to parse
 * @returns
 */
function deserializeEventTarget(target: string): EventTarget {
    if (target === "<window") return window;
    if (target === "<document") return document;
    const element = document.querySelector(target);
    if (element) return element;
    else throw new PostedError(InfoError.Selector_NotFound, `Invalid CSSSelector, selector not found: "${target}"`);
}

/**
 * Replicate user emitted event
 * @param data parameters of event to replicate
 */
function createEvent(data: EventData): Event {
    const event: { [a: string]: any } = new window[data.ctor](data.type, data.options);
    const hackedEvent = {} as Event;
    // enumerating through properties return right properties
    const doubleHackedEvent = Object.create(hackedEvent);
    for (const prop in event) {
        if (event.hasOwnProperty(prop)) {
            doubleHackedEvent[prop] = event[prop];
        } else {
            (hackedEvent as any)[prop] = typeof event[prop] === "function" ? event[prop].bind(event) : event[prop];
        }
    }
    Object.setPrototypeOf(hackedEvent, Object.getPrototypeOf(event));
    doubleHackedEvent.isTrusted = true;

    const target = deserializeEventTarget(data.targetQuery);

    (hackedEvent.target as any) = target;
    return doubleHackedEvent;
}

/**
 * Parse [[EventData]] and  calls to [[fireOnTarget]]
 * @param data parameters of event to emit
 */
function fireEvent(data: EventData) {
    const event = createEvent(data);
    if (!data.capturingTargets && !data.atTarget && !data.bubblingTargets) {
        const target = deserializeEventTarget(data.targetQuery);
        if (target instanceof Node) {
            const y = [];
            let x = target.parentNode as Node | undefined;
            while (x) {
                y.push(x);
                x = x.parentNode as Node;
            }
            y.push(window);

            Object.getPrototypeOf(event).eventPhase = Event.CAPTURING_PHASE;
            fireOnTarget(y.reverse(), data.type, true, event);

            Object.getPrototypeOf(event).eventPhase = Event.AT_TARGET;
            fireOnTarget([target], data.type, true, event);
            fireOnTarget([target], data.type, false, event);

            Object.getPrototypeOf(event).eventPhase = Event.BUBBLING_PHASE;
            fireOnTarget(y.reverse(), data.type, false, event);
        } else {
            Object.getPrototypeOf(event).eventPhase = Event.AT_TARGET;
            fireOnTarget([target], data.type, true, event);
            fireOnTarget([target], data.type, false, event);
        }
    } else {
        if (data.capturingTargets) {
            Object.getPrototypeOf(event).eventPhase = Event.CAPTURING_PHASE;
            fireOnTarget(data.capturingTargets, data.type, true, event);
        }
        if (data.atTarget) {
            Object.getPrototypeOf(event).eventPhase = Event.AT_TARGET;
            fireOnTarget([data.targetQuery], data.type, true, event);
            fireOnTarget([data.targetQuery], data.type, false, event);
        }

        if (data.bubblingTargets) {
            Object.getPrototypeOf(event).eventPhase = Event.BUBBLING_PHASE;
            fireOnTarget(data.bubblingTargets, data.type, false, event);
        }
    }
}

/**
 * Fires supplied event on targets, using detected event listeners
 * @param targets list of target to fire event on
 * @param type type of event
 * @param capture wether it is event in capturing phase or not
 * @param event event to fire
 * @param onPropagationStopped Called on call to `stopPropagation` or `stopImmediatePropagation`,
 *  `true` return value indicate that it is "handled" and should continue.
 *  (`false` is default value without this parametr)
 */
function fireOnTarget(
    targets: Array<string | EventTarget>,
    type: string,
    capture: boolean,
    event: Event,
    onPropagationStopped?: (details: { target: EventTarget; isImmediate: boolean }) => boolean
) {
    let propagationStopped = false;
    let immediatePropagationStopped = false;
    event.stopPropagation = () => (propagationStopped = true);
    event.stopImmediatePropagation = () => (immediatePropagationStopped = true);

    for (const currentQuery of targets) {
        const currentTarget = typeof currentQuery === "string" ? deserializeEventTarget(currentQuery) : currentQuery;
        if (listeners.has(currentTarget)) {
            (event.currentTarget as any) = currentTarget;
            for (const listenerDef of listeners.get(currentTarget) as EventDef[]) {
                if (
                    listenerDef.type === type &&
                    (listenerDef.options !== undefined &&
                        typeof listenerDef.options.capture === "boolean" &&
                        listenerDef.options.capture) === capture
                ) {
                    try {
                        if (typeof listenerDef.listener === "function") {
                            listenerDef.listener.call(currentTarget, event);
                        } else if (listenerDef.listener) {
                            listenerDef.listener.handleEvent(event);
                        }
                    } catch (error) {
                        console.error(error);
                    }
                    if (
                        immediatePropagationStopped &&
                        !(onPropagationStopped && onPropagationStopped({ target: currentTarget, isImmediate: true }))
                    ) {
                        return;
                    }
                }
            }
            if (
                (event.cancelBubble || propagationStopped) &&
                !(onPropagationStopped && onPropagationStopped({ target: currentTarget, isImmediate: false }))
            ) {
                return;
            }
        }
    }
}
