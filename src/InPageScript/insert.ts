import { NODE_ENV, process } from "../_common/typesBS";

/**
 * Downloaded `InPageScript`
 */
let code: string | undefined;

/**
 * Maps tab IDs to listeners
 */
const listenersByTabID = {} as {
    [tabID: number]: (details: chrome.tabs.InjectDetails) => void;
};
// export async function GetInjectedTabIds()=>;
// export async function InjectOnceOnLoad
// const injected = new Set<number>();
/**
 * Listen to reload/redirect on tab and inject InPageScript, inject only once
 * @param tabID ID of chrome tab to inject in
 * @param reload if set to `true` reloads page and inject script (and don't listen anymore)
 */
export async function InjectOnceOnLoad(tabID: number, reload: boolean): Promise<any> {
    if (tabID in listenersByTabID) {
        throw new Error("");
    }

    const url = await new Promise((resolve, reject) => chrome.tabs.get(tabID, resolve)).then(
        tab => (tab as chrome.tabs.Tab).url
    );
    if (url === undefined) {
        throw new Error();
    }

    /**
     * We need to load synchronously `InPageScript.ts`, there is preloaded and then injected as text.
     * It also make possible to easily add something dynamically
     * `document.children[0].append(InPage.script);` is used because head or body could be missing
     * at that time of page load = time of injection
     */
    code =
        code ||
        // todo remove event listener
        `const InPage = {};
        window.addEventListener("${chrome.runtime.id}",msg=>{InPage.port = msg.ports[0]})
        InPage.script = document.createElement("script");
        InPage.script.innerHTML = \`
                (\${(() => {
                    let id = "${chrome.runtime.id}";
                    ${await fetch(chrome.runtime.getURL("./InPageScript/index.js")).then(response => response.text())}
                }).toString()})()\`;
        document.children[0].append(InPage.script);` +
            // todo remove script
            (process.env.NODE_ENV === NODE_ENV.production ? "" : ``);

    const injected = new Promise((resolve, reject) => {
        const inject = (details: chrome.tabs.InjectDetails) => {
            chrome.tabs.executeScript(tabID, { code, runAt: "document_start" }, resolve);
            RemoveInjectOnLoad(tabID);
        };
        listenersByTabID[tabID] = inject;
        chrome.webRequest.onCompleted.addListener(inject, {
            tabId: tabID,
            urls: ["<all_urls>"],
            types: ["main_frame"],
        });
    });
    if (reload) {
        chrome.tabs.reload(tabID, { bypassCache: true });
    }
    return injected;
}

/**
 * Removes listener registered by [[InjectOnceOnLoad]]
 * @param tabID  ID of chrome tab on which it was registered
 */
export function RemoveInjectOnLoad(tabID: number) {
    if (tabID in listenersByTabID) {
        chrome.webRequest.onCompleted.removeListener(listenersByTabID[tabID]);
        delete listenersByTabID[tabID];
    }
}
