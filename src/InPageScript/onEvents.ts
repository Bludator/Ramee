/** 
 * List of on- event properties 
*/
const onEvents = [
    // firefox?
    "onabort",
    // chrome
    // "onanimationcancel",
    // "onanimationend",
    // "onanimationiteration",
    // "onanimationstart",
    "onauxclick",
    "onblur",
    // firefox
    "oncancel",
    "oncanplay",
    "oncanplaythrough",
    "onchange",
    "onclick",
    "onclose",
    "oncontextmenu",
    // firefox
    "oncuechange",
    "ondblclick",
    "ondrag",
    "ondragend",
    "ondragenter",
    // chrome
    // "ondragexit",
    "ondragleave",
    "ondragover",
    "ondragstart",
    "ondrop",
    "ondurationchange",
    "onemptied",
    "onended",
    "onerror",
    "onfocus",
    "ongotpointercapture",
    "oninput",
    "oninvalid",
    "onkeydown",
    "onkeypress",
    "onkeyup",
    "onload",
    "onloadeddata",
    "onloadedmetadata",
    // chrome
    // "onloadend",
    "onloadstart",
    "onlostpointercapture",
    "onmousedown",
    "onmouseenter",
    "onmouseleave",
    "onmousemove",
    "onmouseout",
    "onmouseover",
    "onmouseup",
    // firefox/deprecated
    "onmousewheel",
    "onpause",
    "onplay",
    "onplaying",
    "onpointercancel",
    "onpointerdown",
    "onpointerenter",
    "onpointerleave",
    "onpointermove",
    "onpointerout",
    "onpointerover",
    "onpointerup",
    // experimental
    "onprogress",
    "onratechange",
    "onreset",
    "onresize",
    "onscroll",
    // "onsecuritypolicyviolation",
    "onseeked",
    "onseeking",
    "onselect",
    "onselectionchange",
    "onselectstart",
    "onstalled",
    "onsubmit",
    "onsuspend",
    "ontimeupdate",
    // "ontoggle",
    "ontouchcancel",
    "ontouchend",
    "ontouchmove",
    "ontouchstart",
    // chrome
    // "ontransitioncancel",
    // "ontransitionend",
    // "ontransitionrun",
    // "ontransitionstart",
    "onvolumechange",
    "onwaiting",
    "onwheel",
];
export default onEvents;