import { ErrorMessageData, InfoError } from "./typeDefinitions/InfoError";
/**
 * Parses `Error` object
 * @param error `Error` to parse
 */
export function ParseError(error: Error): ErrorMessageData {
    return { error: InfoError.Unexpected, message: error.message };
}
