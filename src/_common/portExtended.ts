declare module "" {
    global {
        namespace chrome.runtime {
            // tslint:disable-next-line: interface-name
            interface Port {
                connected?: boolean;
            }
        }
        namespace chrome.tabs {
            // tslint:disable-next-line: interface-name
            interface Port {
                /**
                 * if set to true port id connected, false means port is disconnected,
                 * undefined is when port is not yet connected or it is not extended
                 */
                connected?: boolean;
            }
        }
    }
}

ExtendPort(chrome.runtime);
if (chrome.tabs) {
    // CS
    ExtendPort(chrome.tabs);
}
/**
 * Extends connect method so it returns port with `connected` property
 * @param parent object with connect method
 */
function ExtendPort(parent: { connect: typeof chrome.tabs.connect | typeof chrome.runtime.connect }) {
    const original = parent.connect;
    parent.connect = (...args: Parameters<typeof original>) => {
        const port = original(...args);
        // https://bugs.chromium.org/p/chromium/issues/detail?id=836370#c2
        // If fixed add tests for port.connected == undefined
        /* if (chrome.runtime.lastError) {
            // tslint:disable-next-line: no-console
            console.warn(chrome.runtime.lastError);
        } else {*/
        port.connected = true;

        port.onDisconnect.addListener((p: any) => (port.connected = false));
        return port;
    };
}
