import { Record } from "../typeDefinitions/record";
export default interface ICompressor {
    Compress(record: Record): object;
    Decompress(compressed: object): Record;
}
export class NoCompress implements ICompressor {
    public Compress = (record: Record) => record as object;
    public Decompress = (record: object) => record as Record;
}
