export interface IStorable {
    Store(): Promise<void>;
}

export interface IRestorable {
    readonly storageType: string;
    GetInstance: (data?: { [key: string]: any }) => IStorable;
}
