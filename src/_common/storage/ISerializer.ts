import { RawRecord } from "../typeDefinitions/record";
export default interface ISerializer {
    /**
     * Converts [[Record]] into string
     * @param obj record to convert
     */
    Serialize(obj: RawRecord): string;
    /**
     * Converts string into [[Record]]
     * @param str string to convert
     * @param id ID to set on returning [[Record]]
     */
    Deserialize(str: string): RawRecord;
}
