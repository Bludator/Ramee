/**
 * Represents setting
 */
export default interface ISettings {
    lastUpdate: string;
}
/**
 * Combined local and sync settings
 */
export interface IMasterSettings extends ISettings {
    storageSources: { [id: string]: IStorageSource };
}

export interface IStorageSource {
    data?: Array<number | boolean | string>;
    isManaged: boolean;
    isWrite: boolean;
    isRead: boolean;
    type: string;
}
