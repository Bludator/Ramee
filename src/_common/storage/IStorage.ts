import { Metadata, RawRecord, Record, recordId } from "../typeDefinitions/record";
import ISettings from "./ISettings";

export interface IStorageSummary {
    [recordId: string]: Metadata;
}

export default interface IStorage extends IManagedReadStorage, IWriteStorage /*, IUpdatableStorage*/ {
    /**
     * Saves settings
     * @param settings settings to save
     */
    setSettings(settings: ISettings): Promise<void>;

    /**
     * Gets settings
     */
    getSettings(): Promise<ISettings>;

    /**
     * Updates s[[Record]] or record's storage
     * @param id id of the record
     * @param record new data
     */
    UpdateRecord(id: recordId, record: Record): Promise<void>;

    /**
     * Removes record from storage
     * @param id ID of [[Record]]
     */
    RemoveRecord(id: recordId): Promise<void>;

    // todo: OnChange;
}
export interface IManagedReadStorage extends IReadStorage {
    /**
     * Gets summary
     */
    getSummary(): Promise<IStorageSummary>;
    // getSummary(recalculate: boolean): Promise<StorageSummary>;
}

export interface IReadStorage {
    /**
     * Returns [[Record]] with given `id`
     * @param id
     */
    GetRecord(id: recordId): Promise<Record>;
}
export interface IWriteStorage {
    /**
     * Saves new [[Record]]
     * @param record data of Record
     * @returns id
     */
    AddRecord(record: RawRecord): Promise<recordId>;
}
