import { NODE_ENV, process } from "../typeDefinitions/parcel";
import { RawRecord, Record, recordId } from "../typeDefinitions/record";
import StorageError, { ErrorType } from "./error";
import ISettings from "./ISettings";
import IStorage, { IManagedReadStorage, IReadStorage, IStorageSummary } from "./IStorage";

// tslint:disable: max-classes-per-file
export class MergedReadStorage<T extends IReadStorage = IReadStorage> implements IReadStorage {
    protected storages: Promise<T[]>;
    constructor(storages: T[]);
    constructor(getStorages: () => Promise<T[]>);
    constructor(param: T[] | (() => Promise<T[]>));
    constructor(param: T[] | (() => Promise<T[]>)) {
        this.storages = typeof param === "function" ? param() : Promise.resolve(param);
    }
    public async GetRecord(id: recordId): Promise<Record> {
        let record: Record | undefined;
        // todo: IDs on un-managed sources could differ from managed ones
        for (const storage of await this.storages) {
            // update: see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/any
            storage
                .GetRecord(id)
                .then(data => (record = data))
                .catch(() => {});
        }
        if (!record) {
            throw new StorageError(ErrorType.NotFound, "Record not found");
        }
        return record;
    }
}
type MergedSummary<Source> = { [K in keyof IStorageSummary]: IStorageSummary[K] & { source: Source } };
export class MergedManagedReadStorage<T extends IManagedReadStorage = IManagedReadStorage> extends MergedReadStorage<T>
    implements IManagedReadStorage {
    private mergedSummary: MergedSummary<T> = {};

    constructor(storages: T[]);
    constructor(getStorages: () => Promise<T[]>);
    constructor(param: T[] | (() => Promise<T[]>));
    constructor(param: T[] | (() => Promise<T[]>)) {
        super(param);
    }
    public async getSummary(): Promise<MergedSummary<T>> {
        if (Object.keys(this.mergedSummary).length !== 0) return this.mergedSummary;

        for (const source of await this.storages) {
            const summary = (await source.getSummary()) as MergedSummary<T>;
            Object.values(summary).forEach(value => (value.source = source));

            this.mergedSummary = Object.assign(this.mergedSummary, summary);
        }
        return this.mergedSummary;
    }
    public async GetRecord(): Promise<Record[]>;
    public async GetRecord(id: recordId): Promise<Record>;
    public async GetRecord(hosts: string[]): Promise<Record[]>;
    public async GetRecord(param?: string[] | recordId): Promise<Record[] | Record> {
        const summary = await this.getSummary();
        if (typeof param === "string") {
            if (!summary[param]) {
                throw new StorageError(ErrorType.NotFound, "Record not found");
            }
            return summary[param].source.GetRecord(param);
        }
        const set = new Set(param);
        const mergedRecords: Record[] = [];
        for (const id in summary) {
            if (set.has(summary[id].host)) {
                mergedRecords.concat(await this.GetRecord(id));
            }
        }
        return mergedRecords;
    }
}

export default class MergedStorage<T extends IStorage = IStorage> extends MergedManagedReadStorage<T>
    implements IStorage {
    constructor(storages: T[], defaultStorage: T);
    constructor(getStorages: () => Promise<T[]>, defaultStorage: T);
    constructor(param: T[] | (() => Promise<T[]>), public defaultStorage: T) {
        super(param);
    }
    public async setSettings(settings: ISettings, storage: T = this.defaultStorage): Promise<void> {
        await storage.setSettings(settings);

        if (process.env.NODE_ENV === NODE_ENV.development) {
            if (!storage) {
                // tslint:disable-next-line: no-console
                console.warn(
                    "This is probably mistaken....\n" + "To suppress this message specify storage explicitly."
                );
            }
        }
    }
    public async getSettings(): Promise<ISettings>;
    public async getSettings(storage: T): Promise<ISettings>;
    public async getSettings(storage?: T): Promise<ISettings> {
        let settings: ISettings;
        if (storage === undefined) {
            for (const source of await this.storages) {
                const sourceSettings = await source.getSettings();
                if (settings! === undefined || new Date(settings.lastUpdate) < new Date(sourceSettings.lastUpdate)) {
                    settings = sourceSettings;
                }
            }
            if (process.env.NODE_ENV === NODE_ENV.development) {
                // tslint:disable-next-line: no-console
                console.warn(
                    "This is probably mistaken....\n" + "To suppress this message specify storage explicitly."
                );
            }
        } else settings = await (storage as T).getSettings();
        return settings!;
    }
    public async UpdateRecord(id: recordId, record: Record): Promise<void>;
    public async UpdateRecord(id: recordId, record: Record, storage: T): Promise<void>;
    public async UpdateRecord(id: recordId, record: Record, storage?: T): Promise<void> {
        const summary = await this.getSummary();
        if (storage) {
            if (summary[id].source !== storage) {
                summary[id].source.RemoveRecord(id);
                this.AddRecord(record, storage);
            }
        } else {
            summary[id].source.UpdateRecord(id, record);
        }
    }
    public async RemoveRecord(id: recordId): Promise<void> {
        const summary = await this.getSummary();
        if (!summary[id]) {
            throw new StorageError(ErrorType.NotFound, "Record not found");
        }
        summary[id].source.RemoveRecord(id);
    }
    public async AddRecord(record: RawRecord, storage: T = this.defaultStorage): Promise<string> {
        return storage.AddRecord(record);
    }
}
