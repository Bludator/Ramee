import { Record } from "../typeDefinitions/record";
import ICompressor, { NoCompress } from "./ICompressor";
import ISerializer from "./ISerializer";
export default class SerializerJSON implements ISerializer {
    constructor(private compressor: ICompressor = new NoCompress()) {}
    /**
     * Converts [[Record]] into string
     * @param record record to convert
     */
    public Serialize(record: Record): string {
        // todo
        // delete newObj.id;
        return JSON.stringify(this.compressor.Compress(record));
    }
    /**
     * Converts string into [[Record]]
     * @param str string to convert
     */
    public Deserialize(str: string): Record {
        return this.compressor.Decompress(JSON.parse(str));
    }
}
