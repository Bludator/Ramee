/**
 * Wraps native `chrome.storage` api in more convenient functions
 */

import { RawRecord, Record, recordId } from "../typeDefinitions/record";
import GetId from "./idGenerator";
import ISerializer from "./ISerializer";
import ISettings from "./ISettings";
import IStorage, { IStorageSummary } from "./IStorage";
import QuotaError, { ErrorType, QuotaErrors, StorageError } from "./quotaError";

const SUMMARY_ID = "summary";
const SETTINGS_ID = "settings";

export default abstract class BrowserStorage implements IStorage {
    private _summary: IStorageSummary | undefined;

    /**
     * @param location
     * @param serializer
     */
    constructor(private location: "sync" | "local", public serializer: ISerializer) {
        if (new.target === BrowserStorage) {
            throw new TypeError("Cannot construct BrowserStorage instances directly");
        }
    }

    public getSummary(): Promise<IStorageSummary> {
        if (this._summary) {
            return Promise.resolve(this._summary);
        } else {
            return new Promise((resolve, reject) => {
                chrome.storage[this.location].get(SUMMARY_ID, async data => {
                    this.CheckError(reject);
                    if (Object.values(data).length === 0) {
                        resolve({});
                    }
                    resolve(data[SUMMARY_ID]);
                    this.CheckError();
                });
            });
        }
    }

    /**
     * Gets settings
     */
    public async getSettings(): Promise<ISettings> {
        return JSON.parse(await this.Get(SETTINGS_ID));
    }
    /**
     * Saves settings
     * @param settings settings to save
     */
    public async setSettings(settings: ISettings): Promise<void> {
        this.Set(SETTINGS_ID, JSON.stringify(settings));
    }

    /**
     * Returns [[Record]] with given `id`
     * @param id
     */
    public async GetRecord(id: recordId): Promise<Record> {
        return Object.assign(this.serializer.Deserialize(await this.Get(id)), { id });
    }

    /**
     * Saves new [[Record]]
     * @param record data of Record
     * @returns id
     */
    public async AddRecord(record: RawRecord): Promise<string> {
        const summary = await this.getSummary();
        const id = GetId(record);
        summary[id] = record.metadata;
        const dataString = this.serializer.Serialize(record);
        await this.Set(id, dataString);
        this.setSummary(summary);
        return id;
    }

    /**
     * Updates s[[Record]] or record's storage
     * @param id id of the record
     * @param record new data
     */
    public async UpdateRecord(id: recordId, record: Record): Promise<void> {
        const summary = await this.getSummary();

        const dataString = this.serializer.Serialize(record);
        await this.Set(id, dataString);
        summary[id] = record.metadata;
        this.setSummary(summary);
    }

    /**
     * Removes record from storage
     * @param id ID of [[Record]]
     */
    public async RemoveRecord(id: recordId): Promise<void> {
        const summary = await this.getSummary();
        // tslint:disable-next-line: no-null-keyword
        this.Set(id, null);
        delete summary[id];
        this.setSummary(summary);
    }

    protected setSummary(summary: IStorageSummary): void {
        this._summary = summary;
        chrome.storage[this.location].set({ [SUMMARY_ID]: summary }, () => this.CheckError());
    }

    /**
     * Loads data listed b`id`
     * @param id id of item to get
     */
    protected async Get(id: recordId): Promise<string> {
        return new Promise((resolve, reject) => {
            chrome.storage[this.location].get(id, data => {
                this.CheckError(reject);
                if (data[id]) {
                    resolve(data[id] as string);
                } else reject(new StorageError(ErrorType.NotFound, "Item not found."));
            });
        });
    }
    /**
     * Set `data` to sync/local storage listed by `id`
     * @param id id of the record; [[minId,]] [[skip]] and [[meta]] constants
     * @param data data to save
     */
    protected async Set(id: recordId, data: string | null): Promise<void> {
        if (data === null) {
            return new Promise((resolve, reject) => {
                chrome.storage[this.location].remove(id, () => {
                    this.CheckError(reject);
                    resolve();
                });
            });
        } else {
            return new Promise((resolve, reject) => {
                chrome.storage[this.location].set({ [id]: data }, () => {
                    this.CheckError(reject);
                    resolve();
                });
            });
        }
    }

    /**
     * Throws error if there is runtime.lastError set.
     */
    protected CheckError(notThrow?: boolean): StorageError | undefined;
    protected CheckError(reject?: (reason?: any) => void): StorageError | undefined;
    protected CheckError(reject?: boolean | ((reason?: any) => void)): StorageError | undefined {
        let error: StorageError | undefined;
        if (chrome.runtime.lastError) {
            if (chrome.runtime.lastError.message) {
                let quotaError: QuotaErrors | undefined;
                const msg = chrome.runtime.lastError.message.replace(/ .*/, "");
                for (const key of Object.keys(QuotaErrors)) {
                    if (msg === key) {
                        quotaError = (QuotaErrors as any)[key] as QuotaErrors;
                        break;
                    }
                }
                if (typeof quotaError !== "undefined") {
                    error = new QuotaError(quotaError);
                } else {
                    error = new StorageError(ErrorType.Unexpected, chrome.runtime.lastError.message);
                }
            }
            error = error || new StorageError(ErrorType.Unexpected, "non-quota runtime.lastError");
        }
        if (error) {
            if (typeof reject === "function") {
                reject(error);
            }
            if (!reject) {
                throw error;
            }
        }
        return error;
    }
}
