import ISerializer from "./ISerializer";
import IStorage from "./IStorage";
import SerializerJSON from "./SerializerJSON";
import BrowserStorage from "./StorageBrowser";
import BrowserSyncStorage from "./StorageBrowserSync";

const STORAGE_ID = "_BrowserLocalStorage";

export default class BrowserLocalStorage extends BrowserStorage implements IStorage {
    public static storageType = STORAGE_ID;
    public static GetInstance(serializer: ISerializer = new SerializerJSON()) {
        // tslint:disable-next-line: no-null-keyword
        if (this.instance == null) this.instance = new BrowserLocalStorage(serializer);
        return this.instance;
    }
    private static instance: BrowserLocalStorage;
    /**
     *
     */
    private constructor(serializer: ISerializer) {
        super("local", serializer);
    }
    public async Store(): Promise<void> {
        BrowserSyncStorage.GetInstance().SetStorageSource({
            isManaged: true,
            isRead: true,
            isWrite: true,
            type: STORAGE_ID,
        });
    }
}
