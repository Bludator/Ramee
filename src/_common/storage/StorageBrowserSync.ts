import ISerializer from "./ISerializer";
import { IMasterSettings, IStorageSource } from "./ISettings";
import IStorage from "./IStorage";
import SerializerJSON from "./SerializerJSON";
import BrowserStorage from "./StorageBrowser";

const STORAGE_ID = "_BrowserSyncStorage";
export default class BrowserSyncStorage extends BrowserStorage implements IStorage {
    public static storageType = STORAGE_ID;
    public static GetInstance(serializer: ISerializer = new SerializerJSON()) {
        // tslint:disable-next-line: no-null-keyword
        if (this.instance == null) this.instance = new BrowserSyncStorage(serializer);
        return this.instance;
    }
    private static instance: BrowserSyncStorage;
    private sourceID = 0;
    /*
     *
     */
    private constructor(serializer: ISerializer) {
        super("sync", serializer);
    }
    public async Store(): Promise<void> {
        await this.setSettings({
            lastUpdate: new Date(0).toISOString(),
            storageSources: {
                ["0"]: { isManaged: true, isRead: true, isWrite: true, type: STORAGE_ID },
            },
        });
    }
    public async SetStorageSource(source: IStorageSource) {
        const settings = await this.getSettings();
        settings.storageSources[(this.sourceID++).toString()] = source;
        await this.setSettings(settings);
    }
    /**
     * Convert ISettings to IMasterSettings
     * @param settings
     */
    public async getSettings(): Promise<IMasterSettings> {
        return super.getSettings() as Promise<IMasterSettings>;
    }
    /**
     * Convert IMasterSettings to ISettings
     * @param settings
     */
    public async setSettings(settings: IMasterSettings): Promise<void> {
        return super.setSettings(settings);
    }
    public async SynchronizeSettings(storage: IStorage) {
        throw new Error("Method not implemented.");
        // const remote = await storage.getSettings();
        // const local = await this.getSettings();
        // new Date().toISOString()
    }
}
