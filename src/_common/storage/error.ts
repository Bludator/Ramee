/**
 * Storage error types
 */
export enum ErrorType {
    QuotaError,
    NotFound,
    Unexpected,
}

/**
 * Represents any error that could occurs in storage
 */
export default class StorageError extends Error {
    public readonly type: ErrorType;
    constructor(type: ErrorType, message: string) {
        super(message);
        this.type = type;
    }
}
