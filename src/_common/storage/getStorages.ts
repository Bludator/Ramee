import { IStorable } from "./IReStorable";
import { IStorageSource } from "./ISettings";
import IStorage, { IManagedReadStorage, IReadStorage } from "./IStorage";
import BrowserSyncStorage from "./StorageBrowserSync";
import StorageSourceMap from "./storageSourceMap";

export async function GetStorages(): Promise<IStorage[]> {
    return ((await Get(
        (source: IStorageSource) => source.isRead && source.isManaged && source.isWrite
    )) as unknown) as IStorage[];
}

export async function GetManagedReadStorages(): Promise<IManagedReadStorage[]> {
    return ((await Get(
        (source: IStorageSource) => source.isRead && source.isManaged
    )) as unknown) as IManagedReadStorage[];
}

export async function GetReadStorages(): Promise<IReadStorage[]> {
    return ((await Get((source: IStorageSource) => source.isRead)) as unknown) as IReadStorage[];
}

async function Get(filter: (source: IStorageSource) => boolean): Promise<IStorable[]> {
    const sourcesInitializations = (await BrowserSyncStorage.GetInstance().getSettings()).storageSources;
    return Object.values(sourcesInitializations)
        .filter(filter)
        .map(source => StorageSourceMap[source.type].GetInstance(source.data));
}
