// @ts-ignore import
import hash from "object-hash";
import { RawRecord } from "../typeDefinitions/record";
export default function GetId(record: RawRecord): string {
    return hash(record, { encoding: "base64" });
}
