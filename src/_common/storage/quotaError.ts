import StorageError, { ErrorType } from "./error";
export { default as StorageError, ErrorType } from "./error";

/**
 * Lists all storage quota errors types
 */
export enum QuotaErrors {
    QUOTA_BYTES = "QUOTA_BYTES",
    QUOTA_BYTES_PER_ITEM = "QUOTA_BYTES_PER_ITEM",
    MAX_ITEMS = "MAX_ITEMS",
    MAX_WRITE_OPERATIONS_PER_HOUR = "MAX_WRITE_OPERATIONS_PER_HOUR",
    MAX_WRITE_OPERATIONS_PER_MINUTE = "MAX_WRITE_OPERATIONS_PER_MINUTE",
}
/**
 * Represents quota error of storage
 */
export default class QuotaError extends StorageError {
    public readonly quotaError: QuotaErrors;
    constructor(quotaError: QuotaErrors) {
        super(ErrorType.QuotaError, quotaError + " exceeded.");
        this.quotaError = quotaError;
    }
}
