import { IRestorable } from "./IReStorable";
import BrowserLocalStorage from "./StorageBrowserLocal";
import BrowserSyncStorage from "./StorageBrowserSync";

/**
 * Lists Storage types (constructor functions) by IDs
 */
const StorageSourceTypesMap: { [storageType: string]: IRestorable } = {
    [BrowserSyncStorage.storageType]: BrowserSyncStorage as IRestorable,
    [BrowserLocalStorage.storageType]: BrowserLocalStorage as IRestorable,
};
export default StorageSourceTypesMap;
