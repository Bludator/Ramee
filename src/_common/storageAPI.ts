/* 

Example:
```
// Initialize new MergedStorage
let storage = new Storage(GetStorages, new BrowserLocalStorage());

```
*/
// Storage Interfaces
export {
    default as IStorage,
    IManagedReadStorage,
    IReadStorage,
    IStorageSummary as StorageSummary,
} from "./storage/IStorage";

//
export { GetManagedReadStorages, GetReadStorages, GetStorages } from "./storage/getStorages";

// Specific storages:
// MergedStorage as a default
export {
    default as Storage,
    MergedReadStorage as ReadStorage,
    MergedManagedReadStorage as ManagedReadStorage,
} from "./storage/MergedStorage";

export { default as BrowserLocalStorage } from "./storage/StorageBrowserLocal";
export { default as BrowserSyncStorage } from "./storage/StorageBrowserSync";
