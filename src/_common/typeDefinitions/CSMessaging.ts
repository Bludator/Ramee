import "../portExtended";
import { RecMode, TypedPort } from "./global";
import { ErrorMessageData } from "./InfoError";
import * as Record from "./record";

export { ErrorMessageData, InfoError } from "./InfoError";
// tslint:disable interface-name
// tslint:disable class-name

export type BSPort = TypedPort<toBS.Msg, toCS.Msg>;
export type CSPort = TypedPort<toCS.Msg, toBS.Msg>;

export enum ContentScriptType {
    Repeating = "Repeating",
    Recording = "Recording",
}

/**
 * Background script to content script messages
 */
export namespace toCS {
    /**
     * Commands send to CS
     */
    export enum command {
        // recording:
        record,
        stop,
        save,
        // repeating:
        do,
        saveResult,
    }
    // recording:
    /**
     * Basic form of message
     */
    export interface Msg {
        cmd: command;
    }
    /**
     * Message to start recording
     */
    export interface RecordMsg extends Msg {
        cmd: command.record;
        /**
         * Mode of recording
         */
        mode: RecMode;
        /**
         * wether it is starting on tab (`false`) or starting on web address .. after redirect (`true`)
         */
        isResume: boolean;
    }
    /**
     * Additional data specific to form detection
     */
    export interface RecordFormsMsg extends RecordMsg {
        markAll?: boolean;
    }
    /**
     * Additional data specific to event monitoring
     */
    export interface MonitorEventsMsg extends RecordMsg {
        events: string[];
    }

    // repeating:
    /**
     * Message with data to do something
     */
    export interface DoMsg extends Msg {
        cmd: command.do;
        data: Omit<Record.Data, "Init">;
    }

    /**
     * Message to save HTML element as a result
     */
    export interface saveResultMsg extends Msg {
        cmd: command.saveResult;
        /**
         * CSS selector
         */
        result: string;
    }
}
/**
 * Content script to background script messages
 */
export namespace toBS {
    /**
     * Types of message
     */
    export enum msgType {
        init,
        recData,
        save,
        cancelled,
        error,
    }
    /**
     * Basic form of message
     */
    export interface Msg {
        msgType: msgType;
    }
    /**
     * Message with URL and so on
     */
    export interface InitMsg extends Msg {
        msgType: msgType.init;
        data: Record.Init;
    }

    /**
     * Message with recording data
     */
    export interface RecDataMsg extends Msg {
        msgType: msgType.recData;
        data: Record.EventData | Record.FormData[];
    }
    /**
     * Message that confirms saving
     */
    export interface SaveMsg extends Msg {
        msgType: msgType.save;
        /**
         * Name that will be used for the record
         */
        name: string;
    }
    /**
     * Error message
     */
    export interface ErrorMsg extends Msg {
        msgType: msgType.error;
        error: ErrorMessageData;
    }
}
