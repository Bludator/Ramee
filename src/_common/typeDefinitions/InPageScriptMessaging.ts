import "../portExtended";
import { ErrorMessageData } from "./InfoError";
import { EventCtors, EventData } from "./record";
export { InfoError, ErrorMessageData } from "./InfoError";
// tslint:disable interface-name
/**
 * InPageScript script to Content script messages
 */
export namespace toCS {
    /**
     * types of message
     */
    export enum msgType {
        eventData,
        error,
    }
    /**
     * Basic form of message
     */
    export interface Msg {
        msgType: msgType;
    }
    /**
     * Error message
     */
    export interface ErrorMsg extends Msg {
        msgType: msgType.error;
        error: ErrorMessageData;
    }
    /**
     * Logged event
     */
    export interface EventDataMsg extends Msg {
        msgType: msgType.eventData;
        /**
         * ID of event
         */
        id: number;
        type: string;
        /**
         * Constructor of Event
         */
        ctor: EventCtors;
        targetQuery: string; // todo ECSSQuery
        currentTarget: string; // todo ECSSQuery
        /**
         * String, boolean or number  properties of event
         */
        options: { [a: string]: string | boolean | number };
    }
}
/**
 * Content script to InPageScript script messages
 */
export namespace toMS {
    /**
     * Commands to InPageScript
     */
    export enum command {
        recEvents,
        fireEvent,
        stop,
    }
    /**
     * Basic form of message
     */
    export interface Msg {
        cmd: command;
    }
    /**
     * Message with Events types to log
     */
    export interface RecEventMsg extends Msg {
        cmd: command.recEvents;
        events: string[];
    }
    /**
     * Message wit event data to fire event
     */
    export interface FireEventMsg extends Msg {
        cmd: command.fireEvent;
        data: EventData;
    }
}
