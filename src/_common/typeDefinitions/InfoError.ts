/**
 * List of known (+Unexpected) errors
 */
export enum InfoError {
    Unexpected,
    // In Page Script
    Selector_NotFound,
    Selector_CanNotCreate,
    // Storage
    Storage_QuotaPerItem,
    Storage_Quota,
    // BS
    BadTab,
    NotOpenedDevTools,
    PopUp_CSVParseError,
}

// tslint:disable-next-line: interface-name
/**
 * Error log, to be send through messages
 */
export interface ErrorMessageData {
    error: InfoError;
    message?: string;
    name?: string;
}
