// tslint:disable interface-name
// tslint:disable class-name

import "../portExtended";
import { RecMode, TypedPort } from "./global";
import { ErrorMessageData } from "./InfoError";
import { recordId } from "./record";
/**
 *  Background script to Popup messages
 */

export type BSPort = TypedPort<toBS.Msg, toPopup.Msg>;
export type PopupPort = TypedPort<toPopup.Msg, toBS.Msg>;
export namespace toPopup {
    /**
     * States of background script
     */
    export enum state {
        recording,
        repeating,
        paused,
        ready,
        error,
    }
    export type Msg = stateMsg | errorMsg;
    /**
     * Message with state of background script
     */
    export interface stateMsg {
        state: state;
    }

    /**
     * Logs Error
     */
    export interface errorMsg extends stateMsg {
        state: state.error;
        error: ErrorMessageData;
    }
}

/**
 * Popup to Background script messages
 */
export namespace toBS {
    /**
     * Commands send to BS
     */
    export enum command {
        record,
        repeat,
        pause,
        stop,
    }
    /**
     * Basic form of message
     */
    export interface Msg {
        cmd: command;
    }
    /**
     *  Message to start recording
     */
    export interface RecordMsg extends Msg {
        cmd: command.record;
        /**
         * mode of recording
         */
        mode: RecMode;
        /**
         * Event's types to record
         */
        events: string[];
    }
    /**
     * Message to start repeating
     */
    export interface RepeatMsg extends Msg {
        cmd: command.repeat;
        /**
         * How many times to repeat the record
         */
        count: number;
        /**
         * Speed of repeating
         */
        speed: number;
        /**
         * ID of record to repeat
         */
        recordID: recordId;
        /**
         * Data to fill in form elements
         */
        userData?: Array<Array<string | number | boolean>>;
        /**
         * CSSSelector to an element to download
         */
        resultSelector?: string;
    }
}
