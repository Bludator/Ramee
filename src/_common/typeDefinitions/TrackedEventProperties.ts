// tslint:disable: interface-name
/**
 * Properties that are tracked listed as string by constructors
 * The main purpose of this is to export it to JSON, then use as (string) list of properties
 */
export type TrackedEventPropertiesList = { [a in keyof TrackedEventProperties]: keyof TrackedEventProperties[a] };
/**
 * Properties that are tracked listed by constructors
 */
export interface TrackedEventProperties {
    Event: EventProperties;
    UIEvent: UIEventProperties;
    MouseEvent: MouseEventProperties;
    KeyboardEvent: KeyboardEventProperties;
    WheelEvent: WheelEventProperties;
    PointerEvent: PointerEventProperties;
    // TouchEvent: TouchEventProperties;
    // DragEvent: DragEventProperties;
    // ClipboardEvent: ClipboardEventProperties;
    InputEvent: InputEventProperties;
}
interface EventProperties {
    bubbles?: boolean;
    // cancelBubble?: boolean;
    cancelable?: boolean;
    composed?: boolean;
    // currentTarget?: EventTarget | null;
    // defaultPrevented?: boolean;
    // eventPhase?: number;
    // isTrusted?: boolean;
    // returnValue?: boolean;
    // srcElement?: EventTarget | null;
    // target?: EventTarget | null;
    // timeStamp?: number;
    // type?: string;
    // composedPath()?: EventTarget[];
    // preventDefault()?: void;
    // stopImmediatePropagation()?: void;
    // stopPropagation()?: void;
    // AT_TARGET?: number;
    // BUBBLING_PHASE?: number;
    // CAPTURING_PHASE?: number;
    // NONE?: number;
}
interface UIEventProperties extends EventProperties {
    detail?: number;
    // view?: Window;
}

interface MouseEventProperties extends UIEventProperties {
    altKey?: boolean;
    button?: number;
    buttons?: number;
    clientX?: number;
    clientY?: number;
    ctrlKey?: boolean;
    // fromElement?: Element;
    layerX?: number;
    layerY?: number;
    metaKey?: boolean;
    movementX?: number;
    movementY?: number;
    offsetX?: number;
    offsetY?: number;
    pageX?: number;
    pageY?: number;
    // relatedTarget?: EventTarget;
    screenX?: number;
    screenY?: number;
    shiftKey?: boolean;
    // toElement?: Element;
    // which?: number;
    x?: number;
    y?: number;
    // getModifierState(keyArg?: string): boolean;
}
interface KeyboardEventProperties extends UIEventProperties {
    altKey?: boolean;
    // char?: string;
    // charCode?: number;
    code?: string;
    ctrlKey?: boolean;
    key?: string;
    // keyCode?: number;
    location?: number;
    metaKey?: boolean;
    repeat?: boolean;
    shiftKey?: boolean;
    // which?: number;
    // DOM_KEY_LOCATION_JOYSTICK?: number;
    // DOM_KEY_LOCATION_LEFT?: number;
    // DOM_KEY_LOCATION_MOBILE?: number;
    // DOM_KEY_LOCATION_NUMPAD?: number;
    // DOM_KEY_LOCATION_RIGHT?: number;
    // DOM_KEY_LOCATION_STANDARD?: number;
    // getModifierState(keyArg?: string): boolean;
}
interface WheelEventProperties extends MouseEventProperties {
    deltaMode?: number;
    deltaX?: number;
    deltaY?: number;
    deltaZ?: number;
    // DOM_DELTA_LINE?: number;
    // DOM_DELTA_PAGE?: number;
    // DOM_DELTA_PIXEL?: number;
    // getCurrentPoint(element?: Element): void;
}
interface PointerEventProperties extends MouseEventProperties {
    height?: number;
    isPrimary?: boolean;
    pointerId?: number;
    pointerType?: string;
    pressure?: number;
    tangentialPressure?: number;
    tiltX?: number;
    tiltY?: number;
    twist?: number;
    width?: number;
}
// @ts-ignore
interface TouchEventProperties extends UIEventProperties {
    altKey?: boolean;
    // changedTouches?: TouchList;
    ctrlKey?: boolean;
    metaKey?: boolean;
    shiftKey?: boolean;
    // targetTouches?: TouchList;
    // touches?: TouchList;
}
// @ts-ignore
interface DragEventProperties extends MouseEventProperties {
    // dataTransfer?: DataTransfer | null;
}
// @ts-ignore
interface ClipboardEventProperties extends EventProperties {
    // clipboardData?: DataTransfer | null;
}
interface InputEventProperties extends UIEventProperties {
    data?: string | null;
    inputType?: string;
    isComposing?: boolean;
}
