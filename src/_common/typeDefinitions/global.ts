/**
 * Mode of recording
 */
export enum RecMode {
    form,
    event,
}

export type TypedPort<received, sent> = Omit<chrome.runtime.Port, keyof ITypedPort<received, sent>> &
    ITypedPort<received, sent>;
interface ITypedPort<received, sent> {
    postMessage: (message: sent) => void;
    onMessage: chrome.events.Event<(message: received, port: chrome.runtime.Port) => void>;
}
