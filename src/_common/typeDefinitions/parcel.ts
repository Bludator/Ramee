export declare let process: {
    env: {
        NODE_ENV: NODE_ENV;
    };
};

export enum NODE_ENV {
    production = "production",
    development = "development",
}
