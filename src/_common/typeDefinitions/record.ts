import { TrackedEventProperties } from "./TrackedEventProperties";
// tslint:disable: interface-name
export type CSSSelector = string;
export type recordId = string;
export type URL = string;
/**
 * Path part of URL
 */
export type Path = string;

/**
 * Event constructor e.g. MouseEvent,Event
 */
export type EventCtors = keyof TrackedEventProperties;
/**
 * Data of recording
 */
export type Data = EventData | FormData | Init;
/**
 * If it is path of uURL must match
 */
export type ExactPath = boolean;
/**
 * Record as it is saved in storage
 */
export interface RawRecord {
    data: Data[];
    metadata: Metadata;
}

/**
 * Main unit of data
 */
export interface Record extends RawRecord {
    id: recordId;

    source?: any;
}

/**
 * Saved event (so it can be reproduced) with event listeners
 */
export interface EventData {
    type: string;
    ctor: EventCtors;
    targetQuery: CSSSelector;
    /**
     * Listeners on capturing phase
     */
    capturingTargets?: CSSSelector[];
    /**
     * Listeners on bubbling phase
     */
    bubblingTargets?: CSSSelector[];
    /**
     * Fire on listeners on target?
     */
    atTarget?: boolean;
    options?: TrackedEventProperties[EventCtors];
}

/**
 * Saved information about form element
 */
export interface FormData {
    targetQuery: CSSSelector;
    /**
     * Data type or options that could be set to element
     */
    options: string;
    /**
     * Value to fill/set on the element
     */
    value?: string | number | boolean;
    /**
     * Name of the element
     */
    label: string;
}
/**
 * Saved redirect
 */
export interface Init {
    host: URL;
    path: Path;
}

/**
 * Additional data
 */
export interface Metadata {
    /**
     * User selected name
     */
    name: string;
    /**
     * URL without path
     */
    host: URL;
    /**
     * Path part of URL
     */
    path: Path;
    /**
     * wether the record will run on whole domain or just exact path
     */
    exactPath?: boolean;
    [k: string]: any;
}
// todo: Others object to storing
