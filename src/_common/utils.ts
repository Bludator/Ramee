/**
 * Downloads input as @param name
 * @param text text to download
 * @param name name of the downloaded file
 */
export function download(text: string, name: string) {
    const element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    element.setAttribute("download", name);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
