import { ContentScriptType, CSPort } from "../_common/typesBS";

const CSs = new Map<number, () => void>();

interface ConnectInfo extends chrome.tabs.ConnectInfo {
    name: ContentScriptType;
}

/**
 * Finds content script, if not found inject, and initialize connection to it
 * @param tabId tab to connect
 * @param connect additional options to connect to CS
 * @returns port to content script
 */
export default async function Connect(tabId: number, connect: ConnectInfo): Promise<CSPort> {
    if (!CSs.has(tabId)) {
        await new Promise((resolve, reject) => {
            chrome.tabs.executeScript(tabId, { file: "contentScripts/index.js" }, resolve);
        });
        const listener = () => {
            chrome.webRequest.onCompleted.removeListener(CSs.get(tabId)!);
            CSs.delete(tabId);
        };
        CSs.set(tabId, listener);
        chrome.webRequest.onCompleted.addListener(listener, {
            tabId,
            urls: ["<all_urls>"],
            types: ["main_frame"],
        });
    }
    if (chrome.runtime.lastError) {
        throw new Error(chrome.runtime.lastError.message);
    }
    return chrome.tabs.connect(tabId, connect);
}
