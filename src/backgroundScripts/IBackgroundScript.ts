import { ErrorMessageData } from "../_common/typesBS";
/**
 * Interface for background scripts, runs on single tab
 */
export default interface IBackgroundScript {
    /**
     * Indicate wether [[IBackgroundScript]] is paused
     */
    isPaused: boolean;
    /**
     * Callback called when [[IBackgroundScript]] stopped
     */
    stopped?: () => void;
    /**
     * Callback called when error occurred
     */
    onError?: (error: ErrorMessageData) => void;
    /**
     * Starts [[IBackgroundScript]]
     */
    start(): Promise<void>;
    /**
     * Stops [[IBackgroundScript]]
     */
    stop(): Promise<void>;
    /**
     * Pauses [[IBackgroundScript]] and sets [[IBackgroundScript.isPaused]]
     */
    pause(): Promise<void>;
}
