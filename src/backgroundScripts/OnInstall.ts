import { BrowserSyncStorage as Storage } from "../_common/storageAPI";
import {} from "../_common/typesBS";

// Clear all notifications (mainly for debugging purpose)
chrome.notifications.getAll(notifications => {
    for (const id in notifications) {
        if (notifications.hasOwnProperty(id)) {
            chrome.notifications.clear(id);
        }
    }
});
chrome.runtime.onInstalled.addListener(details => {
    switch (details.reason) {
        case "install":
            Install();
            break;
        case "update":
        case "chrome_update":
        case "shared_module_update":
        default:
            break;
    }
});

/**
 * Function to be called on installation
 */
function Install() {
    Storage.GetInstance().Store();
}
// chrome.runtime.onStartup;
