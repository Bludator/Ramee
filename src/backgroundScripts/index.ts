import { fromPopup, InfoError, NODE_ENV, PopupPort, process, toPopup } from "../_common/typesBS";
import IBS from "./IBackgroundScript";
import Recording from "./recording";
import Repeating from "./repeating";

window.addEventListener("error", e => {
    if (process.env.NODE_ENV === NODE_ENV.production) {
        e.preventDefault();
    }
    SendError(e.message)();
});
/**
 * Maps tab ID to [[IBackgroundScript]]
 */
const instances: { [tabID: number]: IBS } = {};

/**
 * port to opened Popup
 */
let popupPort: PopupPort;

/**
 * Sends state to popup
 * @param state State to send
 */
function SendState(state: toPopup.state): void {
    if (popupPort.connected) {
        popupPort.postMessage({ state } as toPopup.stateMsg);
    }
}
/**
 *
 * @param state State to send
 * @returns Returns [[SendState]] function with `state` set t`state`
 */
function SendStateFunction(state: toPopup.state): () => void {
    return () => SendState(state);
}

/**
 *
 * @param errorMsg Error to send
 * @returns function which when called sen`errorMsg` to popup
 */
function SendError(errorMsg: string): (data?: any) => void {
    return (data?: any) => {
        if (popupPort.connected) {
            popupPort.postMessage({
                state: toPopup.state.error,
                error: { error: InfoError.Unexpected, message: errorMsg },
            } as toPopup.errorMsg);
        }
        // tslint:disable-next-line: no-console
        console.error(errorMsg, data);
    };
}

chrome.runtime.onConnect.addListener(port => {
    // Is it the right port
    if (port.name === "popup") {
        popupPort = port;
        // Get id of the tab on which the popup has been opened
        chrome.tabs.query({ currentWindow: true, active: true }, tabs => {
            if (chrome.runtime.lastError) {
                SendError("Bad Tab")(chrome.runtime.lastError);
                return;
            }
            const activeTabId = tabs[0] && tabs[0].id;
            // Occurs when focus is in inspect window
            if (activeTabId === undefined) {
                SendError("Bad Tab: try click any tab.")();
                return;
            }
            if (instances[activeTabId]) {
                if (instances[activeTabId] instanceof Recording) {
                    SendState(toPopup.state.recording);
                } else {
                    SendState(toPopup.state.repeating);
                }
                if (instances[activeTabId].isPaused) {
                    SendState(toPopup.state.paused);
                }
            }
            popupPort.onMessage.addListener((request: fromPopup.Msg) => {
                // Parse incoming messages
                switch (request.cmd) {
                    case fromPopup.command.record:
                        let rec = instances[activeTabId];
                        if (!rec) {
                            rec = instances[activeTabId] = new Recording(activeTabId, request as fromPopup.RecordMsg);
                            rec.stopped = () => {
                                delete instances[activeTabId];
                                SendStateFunction(toPopup.state.ready)();
                            };
                            rec.onError = error => {
                                if (popupPort.connected) {
                                    popupPort.postMessage({ state: toPopup.state.error, error } as toPopup.errorMsg);
                                }
                            };
                        }
                        rec.start().then(SendStateFunction(toPopup.state.recording), SendError("Can't record"));
                        break;
                    case fromPopup.command.pause:
                        instances[activeTabId]
                            .pause()
                            .then(SendStateFunction(toPopup.state.paused), SendError("Can't pause"));
                        break;
                    case fromPopup.command.stop:
                        instances[activeTabId].stop();
                        break;
                    case fromPopup.command.repeat:
                        let play = instances[activeTabId];
                        if (!play) {
                            play = instances[activeTabId] = new Repeating(activeTabId, request as fromPopup.RepeatMsg);
                            play.stopped = () => {
                                delete instances[activeTabId];
                                SendStateFunction(toPopup.state.ready)();
                            };
                            play.onError = error => {
                                if (popupPort.connected) {
                                    popupPort.postMessage({ state: toPopup.state.error, error } as toPopup.errorMsg);
                                }
                            };
                        }
                        play.start().then(SendStateFunction(toPopup.state.repeating), SendError("Can't repeat"));
                        break;
                    default:
                        SendError("Can't parse incoming message")(request);
                        break;
                }
            });
        });
    }
});
