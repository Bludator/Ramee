import { EventManager } from "unsuvent";
import { BrowserSyncStorage, GetStorages, IStorage, Storage } from "../_common/storageAPI";
import {
    ContentScriptType,
    CSPort,
    ErrorMessageData,
    EventData,
    FormData,
    fromCS,
    RecMode,
    toCS,
} from "../_common/typesBS";

import { RawRecord } from "../_common/typeDefinitions/record";
import { InjectOnceOnLoad as HardInjectScript, RemoveInjectOnLoad } from "../InPageScript/insert";
import ConnectCS from "./ConnectCS";
import IBC from "./IBackgroundScript";

/**
 * Represents Recording of any kind in one tab
 */
export default class Recording extends EventManager implements IBC {
    /**
     * Indicated that recording is paused
     */
    public isPaused: boolean = false;
    public onError?: (error: ErrorMessageData) => void;
    public stopped?: () => void = undefined;
    private storage: IStorage;
    private record: RawRecord;
    private mode: RecMode;
    private notificationID?: string;
    private events: string[];
    private port?: CSPort;

    /**
     * Creates new [[Recording]] on tab
     * @param tabId  ID of tab on which the repeating will proceed
     * @param options Sets mod o recording ([[RecMode)]] and additional data
     */
    constructor(readonly tabId: number, options: { mode: RecMode; events: string[] }) {
        super(false);
        this.record = { data: [], metadata: { name: "", host: "", path: "" } };
        this.mode = options.mode;
        this.events = options.events;
        this.storage = new Storage(GetStorages, BrowserSyncStorage.GetInstance());
    }

    /**
     * Pauses [[Recording]]
     */
    public async pause(): Promise<void> {
        this.isPaused = true;
    }

    /**
     * Stops [[Recording]]
     */
    public async stop(): Promise<void> {
        this.isPaused = false;
        if (this.port && this.port.connected) this.port.postMessage({ cmd: toCS.command.save });
    }

    /**
     * Starts [[Recording]] - inject CS, hook up events; multiple consecutive calls results into undefined behavior
     * @param isResume indicates wether this function is called on already started [[Recording]],
     *  after reload or navigation
     */
    public async start(isResume = false): Promise<void> {
        if (this.mode === RecMode.event && !isResume) {
            // todo? resume vs reload fo inject InPageScript
            await HardInjectScript(this.tabId, true);
            await new Promise((resolve, reject) => {
                chrome.tabs.onUpdated.addListener((id, info, tab) => {
                    if (id === this.tabId && info.status === "complete") {
                        resolve();
                    }
                });
            });
        }
        this.port = await ConnectCS(this.tabId, { name: ContentScriptType.Recording });

        this.isPaused = false;
        // Subscribe to port events
        this.on(this.port.onMessage, async (msg: fromCS.Msg) => {
            switch (msg.msgType) {
                case fromCS.msgType.init:
                    if (this.record.metadata.host === "") {
                        this.record.metadata.host = (msg as fromCS.InitMsg).data.host;
                        this.record.metadata.path = (msg as fromCS.InitMsg).data.path;
                        await this.createNotification();
                        break;
                    }
                case fromCS.msgType.recData:
                    const RecDataMsg = msg as fromCS.RecDataMsg;
                    if (!Array.isArray(RecDataMsg.data)) {
                        this.record.data.push(RecDataMsg.data as EventData);
                    } else {
                        this.record.data.push(...(RecDataMsg.data as FormData[]));
                    }
                    break;
                case fromCS.msgType.save:
                    this.record.metadata.name = (msg as fromCS.SaveMsg).name;
                    this.storage.AddRecord(this.record);
                    this.cleanup();
                    break;
                case fromCS.msgType.error:
                    if (this.onError) {
                        this.onError((msg as fromCS.ErrorMsg).error);
                    }
                case fromCS.msgType.cancelled:
                default:
                    this.cleanup();
                    break;
            }
        });
        HardInjectScript(this.tabId, false).then(async () => {
            await new Promise((resolve, reject) => {
                chrome.tabs.onUpdated.addListener((id, info, tab) => {
                    if (id === this.tabId && info.status === "complete") {
                        resolve();
                    }
                });
            });
            this.start(true);
        });
        if (!isResume) {
            // handle removed tab too:
            // this.on(this.port.onDisconnect, this.cleanup);
        }
        // Start recording
        this.port.postMessage({
            cmd: toCS.command.record,
            events: this.events,
            mode: this.mode,
            isResume,
        } as toCS.RecordMsg);
    }
    /**
     * Does clean up, emits [[stopped]]
     */
    private async cleanup(): Promise<void> {
        if (this.notificationID) chrome.notifications.clear(this.notificationID);
        RemoveInjectOnLoad(this.tabId);
        this.off();
        if (this.stopped) this.stopped();
        if (this.port && this.port.connected) this.port.postMessage({ cmd: toCS.command.stop });
    }
    /**
     * create new notification for [[Recording]]
     */
    private async createNotification(): Promise<void> {
        this.notificationID = await new Promise((resolve, reject) => {
            chrome.notifications.create(
                {
                    type: "basic",
                    iconUrl: "./media/icon.jpg",
                    title: "Lorem Ipsum",
                    message: "Recording",
                    contextMessage: "Ctrl+Click to click; Ctrl+Alt+Click to force marking submit button",
                    buttons: [{ title: "Done" }, { title: "Cancel" }],
                    requireInteraction: true,
                },
                resolve
            );
        });
        this.on(chrome.notifications.onButtonClicked, (notificationId: string, buttonIndex: number) => {
            if (notificationId === this.notificationID) {
                if (buttonIndex === 0) {
                    // on Done
                    this.stop();
                } else {
                    // on Cancel
                    this.cleanup();
                }
            }
        });
    }
}
