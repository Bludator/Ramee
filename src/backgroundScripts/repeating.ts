import { EventManager } from "unsuvent";
import { GetManagedReadStorages, IManagedReadStorage, ManagedReadStorage } from "../_common/storageAPI";
import {
    ContentScriptType,
    CSPort,
    ErrorMessageData,
    FormData,
    fromCS,
    fromPopup,
    Init,
    Record,
    toCS,
} from "../_common/typesBS";
import { InjectOnceOnLoad as HardInjectScript } from "../InPageScript/insert";
import ConnectCS from "./ConnectCS";
import IBC from "./IBackgroundScript";

/**
 * Represents one repeating instance (repeating in one tab)
 */
export default class Repeating extends EventManager implements IBC {
    /**
     * Indicated that recording is paused
     */
    public isPaused: boolean = false;
    public onError?: (error: ErrorMessageData) => void;
    public stopped?: () => void = undefined;
    public storage: IManagedReadStorage;
    private notificationID?: string;
    private record?: Record;
    private count: number = 1;
    private speed: number = 12;
    private port?: CSPort;
    private interval?: number;
    private resultSelector?: string;
    private userData?: Array<Array<string | number | boolean>>;

    /**
     * Creates new  [[Repeating]] instance with given [[Record]] ID
     * @param tabId ID of tab on which the repeating will proceed
     * @param options object with parameters of repeating.
     */
    constructor(readonly tabId: number, options: Omit<fromPopup.RepeatMsg, "cmd">) {
        super(false);
        this.count = options.count;
        this.speed = options.speed;
        this.resultSelector = options.resultSelector;
        this.userData = options.userData;
        this.storage = new ManagedReadStorage(GetManagedReadStorages);
        this.storage.GetRecord(options.recordID).then(rec => (this.record = rec));
    }

    /**
     * Starts [[Repeating-]] inject CS, hook up events; starts repeating - emitting messages to CS
     * multiple consecutive calls results into undefined behavior
     */
    public async start() {
        this.isPaused = false;
        if (this.count <= 0) {
            return;
        }
        await this.initialize();
        this.notificationID = await new Promise((resolve, reject) => {
            chrome.notifications.create(
                {
                    type: "progress",
                    iconUrl: "./media/icon.jpg",
                    title: "Lorem Ipsum",
                    message: "Repeating",
                    progress: 0,
                    buttons: [{ title: "Stop" }],
                    // there is a bug in chrome, notification is updated just in notif. menu... :-(
                    // requireInteraction: true,
                },
                resolve
            );
        });
        chrome.notifications.onButtonClicked.addListener(this.onToastButton);
        if (this.record === undefined) {
            // todo race condition with constructor
            return;
        }
        let iteration = 0;
        let item = 0;
        let updating = false;

        let iterator = this.record!.data[Symbol.iterator]();
        this.interval = setInterval(() => {
            if (updating) return;
            // Next command
            const x = iterator.next();
            if (!x.done) {
                chrome.notifications.update(this.notificationID!, {
                    progress: Math.round((iteration / this.count) * 100),
                });
                if ((x.value as Init).host !== undefined) {
                    const init = x.value as Init;
                    chrome.tabs.update(this.tabId, { url: "https://" + init.host + init.path }, async tab => {
                        await this.initialize();
                        updating = false;
                    });
                    return;
                }
                if (this.userData && (x.value as FormData).label !== undefined && this.userData.length > iteration) {
                    (x.value as FormData).value = this.userData[iteration][item];
                    item++;
                }
                this.port!.postMessage({ cmd: toCS.command.do, data: x.value } as toCS.DoMsg);
            } else {
                // Next iteration
                iteration++;
                item = 0;
                if (this.resultSelector) {
                    this.port!.postMessage({
                        cmd: toCS.command.saveResult,
                        result: this.resultSelector,
                    } as toCS.saveResultMsg);
                }
                if (iteration < this.count) {
                    iterator = this.record!.data[Symbol.iterator]();
                } else {
                    // Done
                    this.stop();
                }
            }
        }, Math.floor(1000 / this.speed));
    }

    /**
     * Stops [[Repeating]] , unhooks events, call [[stopped]]
     */
    public async stop() {
        this.isPaused = false;
        if (this.interval) clearInterval(this.interval);
        if (this.notificationID) chrome.notifications.clear(this.notificationID);
        chrome.notifications.onButtonClicked.removeListener(this.onToastButton);
        if (this.port && this.port.connected) this.port.disconnect();
        if (this.stopped) this.stopped();
    }

    /**
     * Pauses [[Repeating]]
     */
    public async pause() {
        this.isPaused = true;
    }

    /**
     * Inject `InPageScript` and CS, handles incoming (from popup) messages
     */
    private async initialize() {
        await HardInjectScript(this.tabId, true);
        await new Promise((resolve, reject) => {
            chrome.tabs.onUpdated.addListener((id, info, tab) => {
                if (id === this.tabId && info.status === "complete") {
                    resolve();
                }
            });
        });

        this.port = await ConnectCS(this.tabId, { name: ContentScriptType.Repeating });
        this.port.onMessage.addListener(msg => {
            switch (msg.msgType) {
                case fromCS.msgType.error:
                    if (this.onError) {
                        this.onError((msg as fromCS.ErrorMsg).error);
                    }
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * Function to be used as listener to notification's buttons
     */
    private onToastButton = (notificationId: string, buttonIndex: number) => {
        if (notificationId === this.notificationID) {
            if (buttonIndex === 0) {
                this.stop();
            }
        }
    };
}
