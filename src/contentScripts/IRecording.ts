import { fromBS } from "../_common/typesCS";
/**
 * Represents recording
 */
export interface IRecording {
    /**
     * start recording
     */
    start(): void;
    /**
     * stop and save
     */
    stop(): void;
    /**
     * abort and remove any recorded data
     */
    abort(): void;
}
export import detectFormOptions = fromBS.RecordFormsMsg;
export import monitorEventsOptions = fromBS.MonitorEventsMsg;
