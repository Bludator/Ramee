import {} from "../_common/typesCS";

// Some hacks because of parcel.js ...:-(
export default Object.getOwnPropertyDescriptor(window, "InPage") !== undefined
    ? (InPage as { script: HTMLScriptElement; port: MessagePort })
    : undefined;
// todo port.start()
