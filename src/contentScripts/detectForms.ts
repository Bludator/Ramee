// @ts-ignore import
import finder from "@medv/finder";
import { BSPort, FormData, toBS } from "../_common/typesCS";
import * as formElements from "./formElements";
import { detectFormOptions, IRecording } from "./IRecording";

/**
 * CSS class for highlighting marked form elements
 */
const markedClass = `${chrome.runtime.id}-markedClass`;
/**
 * CSS class for highlighting marked submit buttons
 */
const markedSubmitBtnClass = `${chrome.runtime.id}-markedSubmitBtnClass`;

/**
 * Save form elements selected by user
 */
export default class DetectForms implements IRecording {
    private submitBtn?: Element;
    private markAll: boolean;
    private styles: HTMLStyleElement;
    private elements = new Map<any, boolean>();
    /**
     * Initialize new instance of [[DetectForms]]
     * @param {Object} [port] background script port
     * @param {boolean} [markAll] If set all form elements will be marked otherwise not
     */
    constructor(private port: BSPort, options: detectFormOptions) {
        this.markAll = options.markAll ? options.markAll : true;

        this.styles = document.createElement("style");
        document.head.appendChild(this.styles);
        // @ts-ignore sheet returns CSSStylesheet!
        this.styles.sheet.insertRule(`.${markedClass} { outline: 3px dashed red !important; }`);
        // @ts-ignore sheet returns CSSStylesheet!
        this.styles.sheet.insertRule(`.${markedSubmitBtnClass} { outline: 3px dashed green !important; }`);
    }

    /**
     * Calls [[abort]] and post detected form elements to BS
     */
    public stop(): void {
        this.abort();
        const data: FormData[] = this.getMarket();
        this.port.postMessage({ msgType: toBS.msgType.recData, data } as toBS.RecDataMsg);
        if (this.submitBtn) {
            this.port.postMessage({
                msgType: toBS.msgType.recData,
                data: {
                    type: "click",
                    ctor: "MouseEvent",
                    targetQuery: finder(this.submitBtn as Element),
                },
            } as toBS.RecDataMsg);
        }
    }

    /**
     * Let the user choose what elements to save by highlighting them
     */
    public start(): void {
        // To disable click - catch all click events
        window.addEventListener("click", this.clickHandler, true);

        // adds element to element Map
        for (const newElement of document.querySelectorAll(formElements.userInputElementsSelector)) {
            if (this.isVisible(newElement as HTMLElement)) {
                this.elements.set(newElement, this.markAll);
                if (this.markAll) {
                    newElement.classList.add(markedClass);
                }
            }
        }

        // finds submit button
        for (const submit of document.querySelectorAll(formElements.actionElementsSelector)) {
            if (this.isVisible(submit as HTMLElement)) {
                this.toggleSubmit(submit);
            }
        }

        // exit prompt
        window.addEventListener("onbeforeunload", this.onBeforeUnloadHandler);
    }

    /**
     * Unhook event listeners, remove added CSS classes
     */
    public abort(): void {
        window.removeEventListener("onbeforeunload", this.onBeforeUnloadHandler);
        window.removeEventListener("click", this.clickHandler, true);
        for (const element of document.querySelectorAll(formElements.userInputElementsSelector)) {
            element.classList.remove(markedClass);
        }
        if (this.submitBtn) this.submitBtn.classList.remove(markedSubmitBtnClass);
        this.styles.remove();
    }

    /**
     * Returns if element is visible
     * @param element element to inspect
     * @see https://stackoverflow.com/a/41698614/5739006
     */
    private isVisible(element: HTMLElement): boolean {
        if (!element.offsetParent) return false;
        const style = getComputedStyle(element);
        if (style.display === "none") return false;
        if (style.visibility !== "visible") return false;
        if (Number(style.opacity) < 0.1) return false;
        if (
            element.offsetWidth +
                element.offsetHeight +
                element.getBoundingClientRect().height +
                element.getBoundingClientRect().width ===
            0
        ) {
            return false;
        }
        const elemCenter = {
            x: element.getBoundingClientRect().left + element.offsetWidth / 2,
            y: element.getBoundingClientRect().top + element.offsetHeight / 2,
        };
        let pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y);
        if (pointContainer === element) return true;
        while (pointContainer!.parentElement) {
            pointContainer = pointContainer!.parentElement;
            if (pointContainer === element) return true;
        }
        return false;
    }

    /**
     * Handles click event - cancel it; if it is form element toggle marked/unmarked;
     * CTRL -> normal click;
     * CTRL + ALT -> force mark as submit button;
     *
     * @param event event to handle
     */
    private clickHandler = (event: MouseEvent) => {
        // Is special?
        if (event.ctrlKey) {
            // forces addSubmitBtn
            if (event.altKey) {
                this.toggleSubmit(event.target as Element);
                event.stopImmediatePropagation();
                event.preventDefault();
            }
            // click
            return;
        }

        let element = event.target as Element;
        const trackedElementsMap = new Set(formElements.elements.map(a => a.toUpperCase()));
        while (!trackedElementsMap.has(element.tagName) && element.parentElement) {
            element = element.parentElement;
        }
        // element is `html` element
        if (document.children[0] !== element) {
            // Is tracked element?
            if (this.elements.has(element)) {
                // toggle
                if (this.elements.get(element)) {
                    this.elements.set(element, false);
                    (event.target as Element).classList.remove(markedClass);
                } else {
                    this.elements.set(element, true);
                    (event.target as Element).classList.add(markedClass);
                }
                event.stopImmediatePropagation();
            } else {
                // Could be submit button?
                if ((element as Element).matches(formElements.actionElementsSelector)) {
                    this.toggleSubmit(element as Element);
                } else {
                    throw new Error("Encountered untracked element.");
                }
                event.stopImmediatePropagation();
            }
        }
        event.preventDefault();
    };

    /**
     * Unmarks/Marks element as submit button
     * @param element Element to be marked as submit button
     */
    private toggleSubmit(element: Element): void {
        if (this.submitBtn === element) {
            this.submitBtn.classList.remove(markedSubmitBtnClass);
            this.submitBtn = undefined;
        } else {
            if (this.submitBtn) {
                this.submitBtn.classList.remove(markedSubmitBtnClass);
            }
            this.submitBtn = element;
            this.submitBtn.classList.add(markedSubmitBtnClass);
        }
    }

    /**
     * Gets markets data and other additional data
     * @returns marked elements
     */
    private getMarket(): FormData[] {
        const data: FormData[] = [];
        for (const pair of this.elements) {
            if (pair[1]) {
                const element = pair[0];
                data.push({
                    label: formElements.getName(element),
                    targetQuery: finder(element),
                    options: formElements.getValueType(element),
                });
            }
        }
        return data;
    }

    /**
     * Cancels unload/redirects
     * @param event
     */
    private onBeforeUnloadHandler(event: BeforeUnloadEvent): void {
        event.preventDefault();
        event.returnValue = "";
    }
}
