import { EventData, fromMS, toMS } from "../_common/typesCS";
import InPage from "./InPageScriptPort";

const port: MessagePort | undefined = InPage?.port;
export function EmitEventNative(data: EventData) {
    if (port === undefined) throw new Error();
    port.postMessage({ cmd: toMS.command.fireEvent, data } as toMS.FireEventMsg);
}
export function EmitEvent(data: EventData) {
    if (port === undefined) throw new Error();
    port.postMessage({ cmd: toMS.command.fireEvent, data } as toMS.FireEventMsg);
}
export function MonitorEventsNative(events: string[], onEvent?: (eventData: fromMS.EventDataMsg) => void) {
    if (port === undefined) throw new Error();
    port.postMessage({ cmd: toMS.command.recEvents, events } as toMS.RecEventMsg);
    //  port.add

    return () => {
        port.postMessage({ cmd: toMS.command.stop } as toMS.Msg);
    };
}
