import finder from "@medv/finder";
import {} from "../_common/typesCS";
import { EmitEventNative } from "./events";

/**
 * Represents object with functions that can handle with values on particular `HTMLElement`
 */
interface IFormElement {
    getValue(element: HTMLElement): any;
    /**
     * gets possible values of given `HTMLElement`
     */
    getValueType(element: HTMLElement): string;
    /**
     * sets given value to the  `HTMLElement`
     */
    setValue(element: any, value: any): void;
}
const FormElements: { [elementName: string]: IFormElement } = {
    input: {
        getValue(element: HTMLInputElement) {
            switch (element.type) {
                case "color":
                case "date":
                case "datetime-local":
                case "email":
                case "month":
                case "number":
                case "password":
                case "range":
                case "search":
                case "tel":
                case "text":
                case "time":
                case "url":
                case "week":
                    return element.value;
                case "checkbox":
                case "radio":
                    return element.checked;
                case "file":
                case "button":
                case "hidden":
                case "image":
                case "reset":
                case "submit":
                    return;
                default:
                    throw Error;
            }
        },
        getValueType(element: HTMLInputElement) {
            switch (element.type) {
                case "color":
                case "date":
                case "datetime-local":
                case "email":
                case "month":
                case "time":
                case "url":
                case "week":
                    return element.type;
                case "password":
                case "search":
                case "tel":
                case "text":
                    return "string";
                case "button":
                case "file":
                case "hidden":
                case "image":
                case "reset":
                case "submit":
                    return "none";
                case "number":
                    return "number";
                case "checkbox":
                case "radio":
                    return "boolean";
                case "range":
                    return element.min || element.max ? `${element.min}..${element.max}` : "number";
                default:
                    throw Error;
            }
        },

        setValue(element: HTMLInputElement, value: any) {
            switch (element.type) {
                case "color":
                case "date":
                case "datetime-local":
                case "email":
                case "month":
                case "number":
                case "password":
                case "range":
                case "search":
                case "tel":
                case "text":
                case "time":
                case "url":
                case "week":
                    element.value = value;
                    break;
                case "checkbox":
                case "radio":
                    element.checked = value;
                    break;
                case "file":
                case "button":
                case "hidden":
                case "image":
                case "reset":
                case "submit":

                default:
                    throw Error;
            }
        },
    },
    textarea: {
        getValue(element: HTMLTextAreaElement) {
            return element.value;
        },
        getValueType(element: HTMLTextAreaElement) {
            return "string";
        },
        setValue(element: HTMLTextAreaElement, value: any) {
            element.value = value;
        },
    },
    select: {
        getValue(element: HTMLSelectElement) {
            const values = Array.from(element.selectedOptions).map(option => option.value);
            return values.length === 1 ? values[0] : values;
        },
        getValueType(element: HTMLSelectElement) {
            // todo: support indexes
            let values = "";
            for (const option of element.options) {
                values += (option as HTMLOptionElement).value + " | ";
            }
            return values.slice(0, -3);
        },
        setValue(element: HTMLSelectElement, value: any) {
            if (value instanceof Array) {
                const optionsToSelect = Array.from(element.options).filter(option => value.includes(option.value));

                for (const option of optionsToSelect) {
                    option.selected = true;
                    // todo Emit event
                }
            } else {
                // todo: support indexes
                const index = Array.from(element.options).findIndex(option => value === option.value);
                element.options[index].selected = true;
                // todo Emit event
            }
        },
    },
};

export function getValue(element: HTMLElement): any {
    if (element.isContentEditable) {
        return element.textContent;
    } else {
        const tagName = element.tagName.toLowerCase();
        return FormElements[tagName].getValue(element);
    }
}
/**
 * gets possible values of given `HTMLElement`
 */
export function getValueType(element: HTMLElement): string {
    if (element.isContentEditable) {
        return "string";
    } else {
        const tagName = element.tagName.toLowerCase();
        return FormElements[tagName].getValueType(element);
    }
}
/**
 * sets given value to the given `HTMLElement`
 */
export function setValue(element: HTMLElement, value: any) {
    if (element.isContentEditable) {
        element.textContent = value;
    } else {
        const tagName = element.tagName.toLowerCase();
        FormElements[tagName].setValue(element, value);

        EmitEventNative({ ctor: "Event", type: "change", targetQuery: finder(element) });
    }
    // change event is not fired on content editable elements
    EmitEventNative({
        ctor: "InputEvent",
        type: "input",
        targetQuery: finder(element),
        options: { inputType: "insertFromPaste", data: typeof value === "string" ? value : undefined },
    });
}

export function isValid(value: any, valueType: string | object): boolean {
    // todo: valueType object with e.g. pattern to match
    if (typeof valueType === "string") {
        switch (valueType) {
            // Common types
            case "string":
                if (typeof value === "string") return true;
                return false;
            case "number":
                if (typeof value === "number") return true;
                return /^-?(?:\d+|\d+\.\d+|\.\d+)(?:[eE][-+]?\d+)?$/.test(value);
            case "boolean":
                if (typeof value === "boolean") return true;
                return /^(?:true)|(?:false)$/.test(value);

            // new HTML5 input types
            case "color":
                return /^#[0-9a-fA-F]{6}$/.test(value);
            case "date":
                return /^\d{4}-[01]\d-[0123]\d$/.test(value);
            case "datetime-local":
                return /^\d{4}-[01]\d-[0123]\dT[012]\d:[0-5]\d$/.test(value);
            case "email":
                return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
                    value
                );
            case "month":
                return /^\d{4}-[01]\d$/.test(value);
            case "time":
                return /^[012]\d:[0-5]\d(?::[0-5]\d)?$/.test(value);
            case "url":
                return /^.+:\/\/.+$/.test(value);
            case "week":
                return /^\d{4}-W[0-5]\d$/.test(value);

            // special and bad
            default:
                // ranges
                if (/^\d*\.\.\d*$/.test(valueType)) {
                    if (typeof value !== "string" && typeof value !== "number") return false;
                    const splits = valueType.split("..");
                    const min = splits[0] === "" ? Number.NEGATIVE_INFINITY : Number(splits[0]);
                    const max = splits[1] === "" ? Number.POSITIVE_INFINITY : Number(splits[1]);
                    const parsed = Number(value);
                    return !Number.isNaN(parsed) && parsed >= min && parsed <= max;
                }
                // options ~ unions
                if (/^\w* \| \w*/.test(valueType)) {
                    if (typeof value !== "string") return false;
                    const options = valueType.split(" | ");
                    return options.includes(value);
                }

                // multi options ~ unions
                if (/^\w* & \w*/.test(valueType)) {
                    if (!(value instanceof Array)) return false;
                    const options = valueType.split(" & ");
                    return value.every(o => typeof o === "string" && options.includes(o));
                }

                throw Error;
        }
    } else {
        throw Error;
    }
}
export function getName(element: HTMLElement): string {
    let label: string | undefined;
    if (element.id) {
        label = (document.querySelector(`label[for="${element.id}"]`) as HTMLLabelElement | null)?.innerText;
    }
    if (!label) {
        let parent = element.parentElement;
        while (parent) {
            if (parent.tagName.toLowerCase() === "label") {
                label = parent.innerText;
                break;
            }
            parent = parent.parentElement;
        }
    }
    if (label) {
    } else if (element.name) label = element.name;
    else if (element.value) label = element.value;
    else if (element.title) label = element.title;
    else if (element.placeholder) label = element.placeholder;
    else if (element.id) label = element.id;
    else label = "unknown";
    // todo:
    // label = label ?? element.name ?? element.value ?? element.title ?? element.placeholder ?? element.id ?? "unknown";
    return `${element.type} - ${label}`;
}
/**
 * elements types
 */
export const elements = ["input", "textarea", "select", "button"];

/**
 * Action elements (like button) selector
 */
export const actionElementsSelector =
    // prettier-ignore
    `button[type="button"],
     button[type="submit"],
     input[type="button"],
     input[type="submit"],
     input[type="image"]`;

/**
 * User input elements (like input) selector
 */
export const userInputElementsSelector =
    // prettier-ignore
    `input:not([type="button"]):not([type="submit"]):not([type="image"]),
     textarea, 
     select, 
     [contenteditable]`;
