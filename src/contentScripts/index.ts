import { ParseError } from "../_common/error";
import { BSPort, ContentScriptType, ErrorMessageData, fromMS, NODE_ENV, process, toBS } from "../_common/typesCS";
import IContentScript from "./IContentScript";
import InPage from "./InPageScriptPort";
import Recording from "./recording";
import Repeating from "./repeating";

InPage?.port.addEventListener("message", e => {
    const msg: fromMS.Msg = (e as MessageEvent).data;

    if (msg.msgType === fromMS.msgType.error) {
        LogError((msg as fromMS.ErrorMsg).error);
    }
});
InPage?.port.addEventListener("messageerror", e => {
    LogError(new Error(e.data));
});

window.addEventListener("error", e => {
    if (process.env.NODE_ENV === NODE_ENV.development) {
        // tslint:disable-next-line: no-debugger
        debugger;
    }
    LogError(e.error);
});
const scripts = new Map<BSPort, IContentScript>();
chrome.runtime.onConnect.addListener((port: BSPort) => {
    try {
        switch ((port.name as unknown) as ContentScriptType) {
            case ContentScriptType.Recording:
                scripts.set(port, new Recording(port));
                break;
            case ContentScriptType.Repeating:
                scripts.set(port, new Repeating(port));
                break;

            default:
                break;
        }
    } catch (error) {
        LogError(port, error);
    }
    port.onDisconnect.addListener(p => {
        scripts.delete(p);
    });
});

function LogError(error: Error | ErrorMessageData): void;
function LogError(port: BSPort, error: Error | ErrorMessageData): void;
function LogError(p: BSPort | Error | ErrorMessageData, e?: Error | ErrorMessageData): void {
    let ports: Iterable<BSPort>;
    let error: ErrorMessageData = p as ErrorMessageData;
    if (e) {
        ports = [p as BSPort];

        if (e instanceof Error) {
            error = ParseError(e);
        } else {
            error = e;
        }
    } else {
        ports = scripts.keys();
        if (p instanceof Error) {
            error = ParseError(p);
        } else {
            error = p as ErrorMessageData;
        }
    }

    for (const port of ports) {
        if (port && port.connected) {
            port.postMessage({
                msgType: toBS.msgType.error,
                error,
            } as toBS.ErrorMsg);
        }
    }
}
