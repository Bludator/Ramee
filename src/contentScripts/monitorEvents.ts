// @ts-ignore .json
import TrackedEventProperties from "../_common/typeDefinitions/TrackedEventProperties.json";
import { BSPort, EventData, fromMS, toBS } from "../_common/typesCS";
import { MonitorEventsNative } from "./events";
import InPage from "./InPageScriptPort";
import { IRecording, monitorEventsOptions } from "./IRecording";

/**
 * Properties of events that will be saved
 */
const EventProperties: { [EventConstructor: string]: { enum: string[] } } =
    TrackedEventProperties.definitions.TrackedEventPropertiesList.properties;

/**
 * Save events emitted by user
 */
export default class MonitorEvents implements IRecording {
    private events: string[];
    private port: MessagePort;
    private lastEventID?: number;
    private lastEvent?: EventData;
    private stopInPage?: () => void;

    /**
     * Initialize new [[MonitorEvents]] instance
     * @param {Object} [BSPort] background script port
     * @param {Object} options Events to record
     */
    constructor(private BSPort: BSPort, options: monitorEventsOptions) {
        this.events = options.events;
        // todo: refactor
        if (!InPage?.port) {
            throw new Error();
        }
        this.port = InPage.port;
        this.port.addEventListener("message", a => {
            const msg: fromMS.Msg = (a as MessageEvent).data;
            switch (msg.msgType) {
                case fromMS.msgType.eventData:
                    this.logEvent(msg as fromMS.EventDataMsg);
                    break;
                case fromMS.msgType.error:
                    // todo api put somewhere
                    this.BSPort!.postMessage({
                        msgType: toBS.msgType.error,
                        error: (msg as fromMS.ErrorMsg).error,
                    } as toBS.ErrorMsg);
                    break;
                default:
                    break;
            }
        });
    }
    /**
     * Start capturing user selected events
     */
    public start(): void {
        this.stopInPage = MonitorEventsNative(this.events);
    }

    /**
     * Stops recording
     */
    public stop(): void {
        if (this.lastEvent) {
            this.BSPort.postMessage({
                msgType: toBS.msgType.recData,
                data: this.lastEvent,
            } as toBS.RecDataMsg);
        }
        this.abort();
    }
    /**
     * Cleanup
     */
    public abort(): void {
        this.stopInPage && this.stopInPage();
    }

    /**
     * Parses event
     * @param event event to be send to BS
     */
    private logEvent(event: fromMS.EventDataMsg): void {
        if (this.lastEventID === undefined || event.id !== this.lastEventID) {
            if (this.lastEvent) {
                this.BSPort.postMessage({
                    msgType: toBS.msgType.recData,
                    data: this.lastEvent,
                } as toBS.RecDataMsg);
            }
            this.lastEvent = { type: event.type, ctor: event.ctor, targetQuery: event.targetQuery, options: {} };
            for (const prop of EventProperties[event.ctor].enum) {
                (this.lastEvent.options! as any)[prop] = event.options[prop];
            }
        }
        switch (event.options.eventPhase) {
            case Event.AT_TARGET:
                this.lastEvent!.atTarget = true;
                break;
            case Event.BUBBLING_PHASE:
                if (!this.lastEvent!.bubblingTargets) {
                    this.lastEvent!.bubblingTargets = [];
                }
                if (!this.lastEvent!.bubblingTargets.includes(event.currentTarget)) {
                    this.lastEvent!.bubblingTargets.push(event.currentTarget);
                }
                break;
            case Event.CAPTURING_PHASE:
                if (!this.lastEvent!.capturingTargets) {
                    this.lastEvent!.capturingTargets = [];
                }
                if (!this.lastEvent!.capturingTargets.includes(event.currentTarget)) {
                    this.lastEvent!.capturingTargets.push(event.currentTarget);
                }
                break;

            default:
                break;
        }
    }
}
