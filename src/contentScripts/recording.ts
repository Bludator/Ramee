import { BSPort, fromBS, InfoError, RecMode, toBS } from "../_common/typesCS";
import DetectForms from "./detectForms";
import IContentScript from "./IContentScript";
import { IRecording } from "./IRecording";
import MonitorEvents from "./monitorEvents";

export default class Recording implements IContentScript {
    /**
     * repeating class set according to wanted mode
     */
    private recording: IRecording | undefined;
    /**
     *
     * @param port Port to background script
     */
    constructor(private readonly port: BSPort) {
        port.onMessage.addListener(async (msg: fromBS.Msg) => {
            switch (msg.cmd) {
                case fromBS.command.record:
                    this.start(msg as fromBS.RecordMsg);
                    break;
                case fromBS.command.save:
                    this.recording?.stop();
                    const name = await showSavePrompt();
                    if (name) {
                        this.port.postMessage({ msgType: toBS.msgType.save, name } as toBS.SaveMsg);
                    } else {
                        this.port.postMessage({ msgType: toBS.msgType.cancelled });
                    }
                case fromBS.command.stop:
                    this.recording?.abort();
                    this.port.disconnect();
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * Constructs [[Recording]] with options
     * @param options options from BS
     */
    private async start(options: fromBS.RecordMsg): Promise<void> {
        if (!options.isResume || window.confirm("Do you want to resume recording?")) {
            // record initial state
            this.port.postMessage({
                msgType: toBS.msgType.init,
                data: {
                    host: window.location.host,
                    path: window.location.pathname,
                },
            } as toBS.InitMsg);

            this.recording =
                options.mode === RecMode.form
                    ? new DetectForms(this.port, options as fromBS.RecordFormsMsg)
                    : new MonitorEvents(this.port, options as fromBS.MonitorEventsMsg);

            try {
                this.recording.start();
            } catch (error) {
                this.port.postMessage({
                    msgType: toBS.msgType.error,
                    error: { error: InfoError.Unexpected },
                } as toBS.ErrorMsg);
            }
        } else {
            this.port.postMessage({ msgType: toBS.msgType.cancelled });
        }
    }
}

/**
 * Shows save-as prompt
 */
async function showSavePrompt(): Promise<string | null> {
    return window.prompt("Save as...");
}
