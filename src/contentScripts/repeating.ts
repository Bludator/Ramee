import { BSPort, EventData, FormData, fromBS } from "../_common/typesCS";
import { download } from "../_common/utils";
import { EmitEventNative } from "./events";
import * as formElements from "./formElements";
import IContentScript from "./IContentScript";

export default class Repeating implements IContentScript {
    /**
     *
     * @param port Port to background script
     */
    constructor(private readonly port: BSPort) {
        port.onMessage.addListener(msg => {
            switch (msg.cmd) {
                case fromBS.command.do:
                    this.playHandler(msg as fromBS.DoMsg);
                    break;
                case fromBS.command.saveResult:
                    const element = document.querySelector((msg as fromBS.saveResultMsg).result);
                    if (element) {
                        download(element.outerHTML, "result.txt");
                    }
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * Parses incoming message from background script and does accordingly
     * @param msg message to parse
     */
    private playHandler(msg: fromBS.DoMsg): void {
        if ((msg.data as EventData).ctor) {
            const eventData = msg.data as EventData;

            EmitEventNative(eventData);
        } else {
            const formData = msg.data as FormData;
            const element = document.querySelector(formData.targetQuery);
            if (element) {
                formElements.setValue(element as HTMLElement, formData.value);
            }
        }
    }
}
