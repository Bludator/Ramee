import Vue from "vue";
import app from "./app.vue";
import BootstrapVue from "bootstrap-vue";

Vue.use(BootstrapVue);

window.addEventListener("DOMContentLoaded", () => {
    window.vm = new Vue({
        el: "#app",
        render: h => h(app),
    });
});
