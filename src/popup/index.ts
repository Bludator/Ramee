"use script";
// @ts-ignore import
import * as CSVParser from "papaparse";
// @ts-ignore import
import $ from "quelib";
// @ts-ignore import
import tag from "tag/dist/tag.esm";
import { GetManagedReadStorages, IManagedReadStorage, ManagedReadStorage } from "../_common/storageAPI";
import { BSPort, ErrorMessageData, fromBS, InfoError, RecMode, toBS } from "../_common/typesPopup";
import events from "./events";

const storage: IManagedReadStorage = new ManagedReadStorage(GetManagedReadStorages);
/**
 * Port to Background script
 */
const port: BSPort = chrome.runtime.connect({ name: "popup" });

port.onMessage.addListener((msg: fromBS.stateMsg) => {
    if (msg.state === fromBS.state.error) {
        showError((msg as fromBS.errorMsg).error);
    }
    if (msg.state) {
        setState(msg.state);
    }
});

/**
 * Data uploaded by user, to be used in forms
 */
let userData: Array<Array<string | number | boolean>> | undefined;
$(() => {
    chrome.tabs.query(
        {
            active: true,
            currentWindow: true,
        },
        tab => {
            if (tab[0] && tab[0].url && !tab[0].url.startsWith("chrome")) {
                const myHostname = new URL(tab[0].url).hostname;
                storage.getSummary().then(records => {
                    if (records) {
                        const select: HTMLSelectElement = $("#recordSelect")[0];
                        // tslint:disable-next-line: forin
                        for (const id in records) {
                            const record = records[id];
                            if (myHostname === record.host) {
                                const opt = tag("option", { value: id }, record.name);
                                select.appendChild(opt);
                            }
                        }
                    }
                });
            } else {
                showError({
                    error: InfoError.BadTab,
                    message: "Make sure that you have focus in non-chrome settings tab.",
                });
            }
        }
    );

    fillOptions();

    $("#playBtn").on("click", () => {
        showHide(["#mainMenu", "#optionsBtn"], ["#optionsMenu", "#backBtn"]);
        if (mode === Mode.recording) {
            port.postMessage({
                cmd: toBS.command.record,
                events: getOptions(),
                mode: $(".recModeSelect")[0].selectedOptions[0].value === "form" ? RecMode.form : RecMode.event,
            } as toBS.RecordMsg);
        } else {
            port.postMessage({
                cmd: toBS.command.repeat,
                count: +$("#repeatCount")[0].value,
                // tslint:disable-next-line:no-bitwise
                speed: 1 << $("#speed")[0].value,
                recordID: $("#recordSelect")[0].selectedOptions[0].value,
                resultSelector: $("#resultSelector")[0].value,
                userData,
            } as toBS.RepeatMsg);
        }
    });
    $("#stopBtn").on("click", () => port.postMessage({ cmd: toBS.command.stop }));

    $("#optionsBtn").on("click", () => showHide(["#optionsMenu", "#backBtn"], ["#mainMenu", "#optionsBtn"]));
    $("#backBtn").on("click", () => showHide(["#mainMenu", "#optionsBtn"], ["#optionsMenu", "#backBtn"]));
    $("#resetBtn").on("click", fillOptions);
    $("#addData").on("change", (e: Event) => {
        if (e.target!.files[0]) {
            CSVParser.parse(e.target!.files[0], {
                dynamicTyping: true,
                complete: (result: { data: any; metadata: any; error: any }, file: File) => {
                    userData = result.data;
                },
                error: (err, file) => {
                    showError({ error: InfoError.PopUp_CSVParseError, message: err.message });
                },
            });
        }
    });
    $("#recordSelect").on("change", (e: Event) => {
        // 0 -> record otherwise repeat
        if (e.target) {
            setMode((e.target as HTMLSelectElement).selectedIndex ? Mode.repeating : Mode.recording);
        }
    });
    // first selected
    $("#recordSelect").on("change", function getReady() {
        setState(fromBS.state.ready);
        $("#recordSelect").off("change", getReady);
    });
    $("#speed").on("change", () => {
        const value = $("#speed").value[0];
        $('label[for="speed"]').innerText = `Speed: ${value === "0" ? "min" : value}`;
    });

    $("#actionBar button").disabled = true;
});

/**
 * Populates list options tabs for recording repeating
 */
function fillOptions() {
    $("#eventSelection").innerHTML = "";
    for (const event of Object.keys(events)) {
        const checkbox = tag("input", { type: "checkbox" });
        checkbox.checked = events[event];
        const label = tag("label", [checkbox, event]);
        $("#eventSelection")[0].appendChild(label);
    }

    $("#repeatCount").value = 1;
    $("#speed").value = 11;
}
/**
 * Mode of execution - recording/repeating
 */
enum Mode {
    recording,
    repeating,
}
/**
 * What will user want to do
 */
let mode: Mode;
//#region Properties like
/**
 * Sets new mode and changes UI
 * @param newMode mode to compare/set
 */
function setMode(newMode: Mode) {
    if (mode !== newMode) {
        mode = newMode;
        switch (mode) {
            case Mode.repeating:
                showHide(".repeatMode", ".recMode");
                break;
            case Mode.recording:
                showHide(".recMode", ".repeatMode");
                break;
            default:
                break;
        }
    }
}

/**
 * state of app (on current tab)
 */
let state: fromBS.state;
/**
 * Sets new state and changes UI
 * @param newState state to set
 */
function setState(newState: fromBS.state) {
    if (state !== newState) {
        state = newState;
        switch (state) {
            case fromBS.state.recording:
            case fromBS.state.repeating:
                showHide(undefined, [".loader"]);
                $("#playBtn").disabled = true;
                $("#stopBtn").disabled = false;
                break;
            case fromBS.state.ready:
                showHide("#recordSelect", ".loader");
                $("#actionBar button").disabled = false;
                $("#stopBtn").disabled = true;
                break;
            default:
                break;
        }
    }
}
//#endregion
// @ts-ignore css...
document.styleSheets[0].insertRule(".hidden { display: none; }");
/**
 * Shows and hides supplied elements
 * @param show CSSSelector(s) to show
 * @param hide CSSSelector(s) to hide
 */
function showHide(show?: string[] | string, hide?: string[] | string) {
    if (show) {
        show = Array.isArray(show) ? show : [show];
        show.forEach(query => {
            $(query).classList.remove("hidden");
        });
    }
    if (hide) {
        hide = Array.isArray(hide) ? hide : [hide];
        hide.forEach(query => {
            $(query).classList.add("hidden");
        });
    }
}

/**
 * Gets selected events types
 */
function getOptions() {
    const selectedOptions = [];
    for (const option of $("#eventSelection").children[0]) {
        if (option.children[0].checked) {
            selectedOptions.push(option.innerText);
        }
    }
    return selectedOptions;
}
/**
 * Shows error message
 * @param error Error to show
 */
function showError(error: ErrorMessageData) {
    showHide("#error", ["#optionsMenu", "#mainMenu"]);
    $("#actionBar button").disabled = true;
    $("#error h2").innerText = error.name ? error.name : InfoError[error.error];
    $("#error p").innerText = error.message;
}
