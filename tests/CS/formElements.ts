// mock dependency (from inside)
const eventsMock = { EmitEventNative: jest.fn(), EmitEvent: jest.fn() };
jest.mock("../../src/contentScripts/events", () => {
    return eventsMock;
});

// imported:
let getValue: (element: HTMLElement) => any;
let getValueType: (element: HTMLElement) => string;
let setValue: (element: HTMLElement, value: any) => void;
let isValid: (value: any, valueType: string | object) => boolean;
let getName: (element: HTMLElement) => string;

declare let __dirname: string;
declare let require: (x: string) => any;

beforeAll(() => {
    // innerText is not implemented by JSDOM :-(
    // https://github.com/jsdom/jsdom/issues/1245#issuecomment-470192636

    // tslint:disable-next-line
    const sanitizeHtml = require("sanitize-html");
    Object.defineProperty(window.Element.prototype, "innerText", {
        get() {
            return sanitizeHtml(this.textContent, {
                allowedTags: [], // remove all tags and return text content only
                allowedAttributes: {}, // remove all tags and return text content only
            });
        },
        configurable: true, // make it so that it doesn't blow chunks on re-running tests with things like --watch
    });
});

// tslint:disable-next-line
const html = require("fs").readFileSync(__dirname + "/formElements.html", "utf8");

// to not run the next beforeEach for `isValid()` block
describe("", () => {
    beforeEach(async () => {
        // imports before each test, prevents side-effects
        ({ getValueType, setValue, isValid, getValue, getName } = await import(
            "../../src/contentScripts/formElements"
        ));
        // sets up html
        document.documentElement.innerHTML = html;

        // contentEditable is not implemented by JSDOM :-(
        (document.getElementById("contentEditable") as any).isContentEditable = true;
        document.getElementById("contentEditable")!.contentEditable = "true";
    });
    describe("getValueType(element) ~ Gets value type in Typescript-like syntax", () => {
        test.each([
            // As of now kind of not worth it except for select and ranges...
            ["input", "string"],
            ["inputButton", "none"],
            ["inputEmail", "email"],
            ["inputNumber", "number"],

            ["inputRadio", "boolean"], // todo? add possible values?

            ["inputRange", "number"],
            ["inputRangeMin", "10.."],
            ["inputRangeMax", "..20"],
            ["inputRangeMinMax", "10..20"],

            ["textarea", "string"],
            ["select", " | a | b | x | y"],
            ["selectMultiple", " & a & b & x & y"],
            ["contentEditable", "string"],
        ])("%s has values of type `%s`", (elementId: string, valueType: string) => {
            expect(getValueType(document.getElementById(elementId)!)).toBe(valueType);
        });
    });

    describe("getValue(element) ~ Gets appropriate value", () => {
        test.each([
            ["contentEditable", "Once upon a time"],
            ["inputText", "Lorem"],
            ["inputCheckboxChecked", true],
            ["inputButton", undefined],
            ["textarea", "Lorem ipsum"],
            // useless except maybe for select?
            ["select", "a"],
            ["selectMultiple", ["a", "x"]],
        ])("%s has value of `%p`", (elementId: string, value: any) => {
            expect(getValue(document.getElementById(elementId)!)).toEqual(value);
        });
    });

    describe("setValue(element, value) ~ Sets value", () => {
        test.each([
            ["textarea", "Lorem ipsum"],
            ["input", "Lorem ipsum"],
            ["select", "a"],
        ])("To %s sets `%s`", (id: string, valueToSet: string) => {
            // A
            const element = document.getElementById(id) as HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement;

            // A
            setValue(element, valueToSet);

            // A
            expect(eventsMock.EmitEventNative).toBeCalledTimes(2);
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "input", ctor: "InputEvent" })
            );
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "change", ctor: "Event" })
            );
            expect(element.value).toBe(valueToSet);
        });
        test("inputCheckbox sets checked to true", () => {
            // A
            const element = document.getElementById("inputCheckbox") as HTMLInputElement;

            // A
            setValue(element, true);

            // A
            expect(eventsMock.EmitEventNative).toBeCalledTimes(2);
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "input", ctor: "InputEvent" })
            );
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "change", ctor: "Event" })
            );
            expect(element.checked).toBe(true);
        });
        test("To contentEditable sets `Foo Bar`", () => {
            // A
            const element = document.getElementById("contentEditable") as HTMLInputElement;
            const value = "Foo Bar";

            // A
            setValue(element, value);

            // A
            expect(eventsMock.EmitEventNative).toBeCalledTimes(1);
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "input", ctor: "InputEvent" })
            );
            expect(element.textContent).toBe(value);
        });
        test("To select with multiple options array checks multiple options", () => {
            // A
            const options = ["a", "x"];
            const element = document.getElementById("selectMultiple") as HTMLSelectElement;

            // A
            setValue(element, options);

            // A
            expect(eventsMock.EmitEventNative).toBeCalledTimes(2 * options.length);
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "input", ctor: "InputEvent" })
            );
            expect(eventsMock.EmitEventNative).toBeCalledWith(
                expect.objectContaining({ type: "change", ctor: "Event" })
            );
            expect((document.getElementById("optionAM") as HTMLOptionElement).selected).toBe(true);
            expect((document.getElementById("optionAM") as HTMLOptionElement).selected).toBe(true);
        });
    });

    describe("getName(element) ~ Gets name of element name in format: `{type} - {name}`", () => {
        test.each([
            ["select", "select-one - Choose a pet:"],
            ["input", "text - Type here: "],
            ["button", "submit - button"],
        ])("%s has name: `%s`", (id: string, name: string) => {
            expect(getName(document.getElementById(id)!)).toBe(name);
        });
    });
});
describe("isValid(value, valueType) ~ Validate value based on value type format from getValueType()", () => {
    test.each([
        ["test", "string", true],
        [10, "string", false],
        [{ a: "test" }, "string", false],

        [10, "number", true],
        [{ a: "test" }, "number", false],
        ["10", "number", true],
        ["test", "number", false],

        [true, "boolean", true],
        [{ a: "test" }, "boolean", false],
        ["true", "boolean", true],
        ["test", "boolean", false],

        ["#19ABef", "color", true],
        ["#123", "color", false],
        ["#qwerty", "color", false],
        [{ a: "test" }, "color", false],

        ["1234-12-31", "date", true],
        ["1234-30-1", "date", false],
        ["test", "date", false],
        [{ a: "test" }, "date", false],

        ["1234-12-31T12:45", "datetime-local", true],
        ["1234-30-1T12:45", "datetime-local", false],
        ["1234-12-31T32:65", "datetime-local", false],
        ["test", "datetime-local", false],
        [{ a: "test" }, "datetime-local", false],

        ["1234@example.com", "email", true],
        ["1234@example", "email", true],
        ["test", "email", false],
        [{ a: "test" }, "email", false],

        ["4234-12", "month", true],
        ["1234-30", "month", false],
        ["test", "month", false],
        [{ a: "test" }, "month", false],

        ["12:34:30", "time", true],
        ["12:34", "time", true],
        ["42:34:12", "time", false],
        ["12", "time", false],
        ["test", "time", false],
        [{ a: "test" }, "time", false],

        ["http://example.com", "url", true],
        ["example.com", "url", false],
        ["test", "url", false],
        [{ a: "test" }, "url", false],

        ["4234-W12", "week", true],
        ["4234-W62", "week", false],
        ["test", "week", false],
        [{ a: "test" }, "week", false],

        ["15", "10..20", true],
        [18, "10..20", true],
        [10, "10..20", true],
        [20, "10..20", true],
        [200, "10..", true],
        [-10, "..20", true],
        [0, "10..20", false],
        [21, "10..20", false],
        ["test", "10..20", false],
        [{ a: "test" }, "10..20", false],

        ["a", "a | b | c", true],
        ["c", "a | b | c", true],
        ["", " | a | b | c", true],
        ["x", "a | b | c", false],
        [["a", "b"], "a | b | c", false],
        [{ a: "test" }, "a | b | c", false],

        [["a", "b"], "a & b & c", true],
        [["b", "", "a"], " & a & b & c", true],
        [["x", "y"], "a & b & c", false],
        [["a", "b", "x"], "a & b & c", false],
        [[15, 18], "a & b & c", false],
        ["test", "a & b & c", false],
    ])("`%p` is of type `%s`: %p", (value: any, valueType: string | object, pass: boolean) => {
        expect(isValid(value, valueType)).toBe(pass);
    });
    test("unknown valueType throws", () => {
        expect(() => isValid("value", "valueType")).toThrow();
    });
});
