import "jest-extended";
import { ErrorMessageData, InfoError, toCS, toMS } from "../../src/_common/typeDefinitions/InPageScriptMessaging";
import { EventData } from "../../src/_common/typeDefinitions/record";
import "../testHelpers/Matchers";

declare let __dirname: string;
declare let require: (x: string) => any;
// tslint:disable-next-line
const html: string = require("fs").readFileSync(__dirname + "/InPageScript.html", "utf8");

const id = "111"; // todo
let MessageChannelMock: MessageChannel;
let sendMessage: (e: MessageEvent) => void;

beforeEach(() => {
    // sets up html
    document.documentElement.innerHTML = html;

    MessageChannelMock = {
        port1: {
            // tslint:disable-next-line: no-null-keyword
            onmessage: null,
            addEventListener: jest.fn(),
            close: jest.fn(),
            postMessage: jest.fn(),
            start: jest.fn(),
            removeEventListener: jest.fn(),
            onmessageerror: jest.fn(),
            dispatchEvent: jest.fn(),
        } as MessagePort,
        port2: {
            // tslint:disable-next-line: no-null-keyword
            onmessage: null,
            addEventListener: jest.fn(),
            close: jest.fn(),
            postMessage: jest.fn(),
            start: jest.fn(),
            removeEventListener: jest.fn(),
            onmessageerror: jest.fn(),
            dispatchEvent: jest.fn(),
        } as MessagePort,
    } as MessageChannel;
    (window as any).id = "111";
    window.MessageChannel = jest.fn(() => MessageChannelMock);
});

test("Creates and sends message port to CS", async () => {
    // A
    window.dispatchEvent = jest.fn();
    // A
    await import("../../src/InPageScript/index");
    // A
    expect(window.dispatchEvent).toBeCalledWith(
        expect.objectContaining({ type: id, ports: [MessageChannelMock.port2] })
    );
});

describe("", () => {
    /**
     * Create default click FireEventMsg
     * @param optionalProperties properties of EventData excluding `type`, `ctor`, `targetQuery`
     */
    function GetFireEventMessage(optionalProperties?: any): { data: toMS.FireEventMsg } {
        const data = Object.assign(
            {
                type: "click",
                ctor: "MouseEvent",
                targetQuery: "#target",
            },
            optionalProperties
        );
        return {
            data: {
                cmd: toMS.command.fireEvent,
                data,
            },
        };
    }
    /**
     * AddEventListener to specified node and return mock handler
     * @param nodeId id of node to register listener
     * @param type event type
     * @param capture
     */
    function addMockHandler(nodeId: string, type: string, capture: boolean = false) {
        const mock = jest.fn();
        document.getElementById(nodeId)!.addEventListener(type, mock, capture);
        return mock;
    }
    beforeEach(async () => {
        // Run side-effects ~ prototype changes etc.
        await import("../../src/InPageScript/index");
        sendMessage = MessageChannelMock.port1.onmessage!;
    });

    describe("Event system should not be affected by side-effects", () => {
        describe("Event registered via on__ event-property listeners", () => {
            test("Raises just appropriate listeners", () => {
                // A
                const onTarget = (document.getElementById("target")!.onclick = jest.fn());
                const notRaisedEvent = (document.getElementById("target")!.onmousemove = jest.fn());

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));
                document.getElementById("target")!.dispatchEvent(new KeyboardEvent("keydown", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);
                expect(notRaisedEvent).not.toBeCalled();
            });
            test("Bubbles", () => {
                // A
                const onTarget = (document.getElementById("target")!.onclick = jest.fn());
                const onParent = (document.getElementById("parent")!.onclick = jest.fn());
                const onGrandparent = (document.getElementById("grandparent")!.onclick = jest.fn());

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);

                expect(onParent).toBeCalledTimes(1);
                expect(onParent).toHaveBeenCalledAfter(onTarget);

                expect(onGrandparent).toBeCalledTimes(1);
                expect(onGrandparent).toHaveBeenCalledAfter(onParent);
            });

            test("Overrides event listener", () => {
                // A
                const overriddenOnParent = (document.getElementById("parent")!.onclick = jest.fn());
                const onParent = (document.getElementById("parent")!.onclick = jest.fn());

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onParent).toBeCalledTimes(1);
                expect(overriddenOnParent).not.toBeCalled();
            });
            test("Raises with correct event object", () => {
                // A
                const onTarget = (document.getElementById("target")!.onclick = jest.fn());
                const event = new MouseEvent("click", { bubbles: true });

                // A
                document.getElementById("target")!.dispatchEvent(event);

                // A
                expect(onTarget).toBeCalledTimes(1);
                expect(onTarget.mock.calls[0][0]).toBe(event);
            });
        });

        describe("Events registered via AddEventListener", () => {
            test("Raises just appropriate listeners ", () => {
                // A
                const onTarget = addMockHandler("target", "click");
                const notRaisedEvent = addMockHandler("target", "mousemove");

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));
                document.getElementById("target")!.dispatchEvent(new KeyboardEvent("keydown", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);
                expect(notRaisedEvent).not.toBeCalled();
            });
            test("Bubbles", () => {
                // A
                const onTarget = addMockHandler("target", "click");
                const onParent = addMockHandler("parent", "click");
                const onGrandparent = addMockHandler("grandparent", "click");

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);

                expect(onParent).toBeCalledTimes(1);
                expect(onParent).toHaveBeenCalledAfter(onTarget);

                expect(onGrandparent).toBeCalledTimes(1);
                expect(onGrandparent).toHaveBeenCalledAfter(onParent);
            });
            test("Captures", () => {
                // A
                const onTarget = addMockHandler("target", "click", true);
                const onParent = addMockHandler("parent", "click", true);
                const onGrandparent = addMockHandler("grandparent", "click", true);

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);

                expect(onParent).toBeCalledTimes(1);
                expect(onParent).toHaveBeenCalledBefore(onTarget);

                expect(onGrandparent).toBeCalledTimes(1);
                expect(onGrandparent).toHaveBeenCalledBefore(onParent);
            });
            test("Supports multiple listeners", () => {
                // A
                const onParent = addMockHandler("parent", "click");
                const onParent2 = addMockHandler("parent", "click");

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onParent).toBeCalledTimes(1);
                expect(onParent2).toBeCalledTimes(1);
                expect(onParent2).toHaveBeenCalledAfter(onParent);
            });
            test("Supports removeEventListener", () => {
                // A
                const onTarget = addMockHandler("target", "click");
                const onParent = addMockHandler("parent", "click");
                const removed = addMockHandler("parent", "click");
                const onParent2 = addMockHandler("parent", "click");

                // A
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

                // A
                expect(onTarget).toBeCalledTimes(1);
                expect(onParent).toBeCalledTimes(1);
                expect(onParent2).toBeCalledTimes(1);
                expect(removed).not.toBeCalled();
            });
            test("Raises with correct event object", () => {
                // A
                const onTarget = addMockHandler("target", "click");
                const event = new MouseEvent("click", { bubbles: true });

                // A
                document.getElementById("target")!.dispatchEvent(event);

                // A
                expect(onTarget).toBeCalledTimes(1);
                expect(onTarget.mock.calls[0][0]).toBe(event);
            });
        });
    });

    describe("Event emitting", () => {
        test("Emits just correct event", () => {
            // A
            const onTarget = addMockHandler("target", "click");
            const notRaisedEvent = addMockHandler("target", "mousemove");

            // A
            sendMessage(new MessageEvent("", GetFireEventMessage()));

            // A
            expect(onTarget).toBeCalledTimes(1);
            expect(notRaisedEvent).not.toBeCalled();
        });
        test("Doesn't call removed listeners (added via `addEventListener`)", () => {
            // A
            const onTarget = addMockHandler("target", "click");
            const notRaisedEvent = addMockHandler("parent", "click");

            // A
            document.getElementById("parent")!.removeEventListener("click", notRaisedEvent, false);
            sendMessage(new MessageEvent("", GetFireEventMessage()));

            // A
            expect(onTarget).toBeCalledTimes(1);
            expect(notRaisedEvent).not.toBeCalled();
        });
        test("Doesn't call removed listeners (added via `on_ event property`)", () => {
            // A
            const onTarget = addMockHandler("target", "click");
            const notRaisedEvent = addMockHandler("parent", "click");
            const notRaisedEvent2 = addMockHandler("grandparent", "click");

            // A
            // tslint:disable-next-line: no-null-keyword
            document.getElementById("parent")!.onclick = null;
            (document.getElementById("grandparent")!.onclick as any) = undefined;
            sendMessage(new MessageEvent("", GetFireEventMessage()));

            // A
            expect(onTarget).toBeCalledTimes(1);
            expect(notRaisedEvent).not.toBeCalled();
            expect(notRaisedEvent2).not.toBeCalled();
        });
        test("Doesn't call `once` event twice", () => {
            // A
            const mock = jest.fn();
            document.getElementById("target")!.addEventListener("click", mock, { once: true });

            // A
            sendMessage(new MessageEvent("", GetFireEventMessage()));
            sendMessage(new MessageEvent("", GetFireEventMessage()));

            // A
            expect(mock).toBeCalledTimes(1);
        });
        test("Doesn't call already fired `once` event", () => {
            // A
            const mock = jest.fn();
            document.getElementById("target")!.addEventListener("click", mock, { once: true });

            // A
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));
            mock.mockReset();
            sendMessage(new MessageEvent("", GetFireEventMessage()));
            sendMessage(new MessageEvent("", GetFireEventMessage()));

            // A
            expect(mock).not.toBeCalled();
        });

        test.todo("test addEventHandler with eventHandler object instead of function");

        test.each([
            [
                // prettier-ignore
                "EventData without targets for emitting event ~ " 
                + "emits event on all capturing listeners",
                GetFireEventMessage(),
                [
                    { listener: { id: "grandparent", capture: true }, called: true },
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "EventData without targets for emitting event ~ " 
                + "emits event on all bubbling listeners",
                GetFireEventMessage(),
                [
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: false }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `atTarget = false` ~ " 
                + "emits event on all (capturing and bubbling) listeners",
                GetFireEventMessage({ atTarge: false }),
                [
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: true },
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `capturingTargets = false` ~ " 
                + "emits event on all (capturing and bubbling) listeners",
                GetFireEventMessage({ capturingTargets: [] }),
                [
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: true },
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `bubblingTargets = []` ~ " 
                + "emits event on all (capturing and bubbling) listeners",
                GetFireEventMessage({ bubblingTargets: [] }),
                [
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: true },
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `atTarget` ~ " 
                + "emits event on target only (capturing and bubbling)",
                GetFireEventMessage({ atTarge: true }),
                [
                    { listener: { id: "parent", capture: true }, called: false },
                    { listener: { id: "target", capture: true }, called: true },
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: false },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `bubblingTargets` ~ " 
                + "emits event on specified bubbling listeners",
                GetFireEventMessage({ bubblingTargets: ["#parent"] }),
                [
                    { listener: { id: "parent", capture: true }, called: false },
                    { listener: { id: "target", capture: true }, called: false },
                    { listener: { id: "target", capture: false }, called: false },
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: true }, called: false },
                ],
            ],
            [
                // prettier-ignore
                "EventData specify just `capturingTargets` ~ " 
                + "emits event on specified capturing listeners",
                GetFireEventMessage({ capturingTargets: ["#parent"] }),
                [
                    { listener: { id: "grandparent", capture: false }, called: false },
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: false },
                    { listener: { id: "target", capture: false }, called: false },
                    { listener: { id: "parent", capture: false }, called: false },
                ],
            ],
            [
                // prettier-ignore
                "Overridden on__ event-property listeners ~ " 
                + "calls last registered listener",
                GetFireEventMessage(),
                [
                    { listener: { id: "parent", isOnEventListener: true }, called: false },
                    { listener: { id: "parent", isOnEventListener: true }, called: false },
                    { listener: { id: "parent", isOnEventListener: true }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "Multiple capturing listeners registered ~ " 
                + "emits events in order in which the corresponding listeners are registered",
                GetFireEventMessage(),
                [
                    { listener: { id: "grandparent", capture: true }, called: true },
                    { listener: { id: "grandparent", capture: true }, called: true },
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "parent", capture: true }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "Multiple bubbling listeners registered ~ " 
                + "emits events in order in which the corresponding listeners are registered",
                GetFireEventMessage(),
                [
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: false }, called: true },
                ],
            ],
            [
                // prettier-ignore
                "Multiple listeners registered, mixed capturing and bubbling~ " 
                + "emits events in order in which the corresponding listeners are registered",
                GetFireEventMessage(),
                [
                    { listener: { id: "grandparent", capture: false }, called: true, order: 6 },
                    { listener: { id: "target", capture: true }, called: true, order: 3 },
                    { listener: { id: "parent", capture: true }, called: true, order: 1 },
                    { listener: { id: "target", capture: false }, called: true, order: 4 },
                    { listener: { id: "parent", capture: false }, called: true, order: 5 },
                    { listener: { id: "parent", capture: true }, called: true, order: 2 },
                    { listener: { id: "grandparent", capture: true }, called: true, order: 0 },
                    { listener: { id: "grandparent", capture: false }, called: true, order: 7 },
                ],
            ],
            [
                // todo check specs: may be chrome specific
                // prettier-ignore
                "Multiple listeners registered, mixed 'addEventListener' and on__ event-property listeners ~ " 
                + "calls listeners in correct order (= registration order)",
                GetFireEventMessage(),
                [
                    { listener: { id: "parent", capture: true }, called: true },
                    { listener: { id: "target", capture: true }, called: true },
                    { listener: { id: "target", isOnEventListener: true }, called: true },
                    { listener: { id: "target", capture: false }, called: true },
                    { listener: { id: "parent", isOnEventListener: true }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "parent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: false }, called: true },
                    { listener: { id: "grandparent", capture: false }, called: true },
                    { listener: { id: "grandparent", isOnEventListener: true }, called: true },
                ],
            ],
            [
                // todo check specs: may be chrome specific
                // prettier-ignore
                "Multiple listeners registered, mixed 'addEventListener' and overridden on__ event-property listeners ~ " 
                + "calls listeners in place of the first registration",
                GetFireEventMessage(),
                [
                    { listener: { id: "target", isOnEventListener: true }, called: true, order: 0 },
                    { listener: { id: "parent", isOnEventListener: true }, called: false },
                    { listener: { id: "parent", capture: false }, called: true, order: 2 },
                    { listener: { id: "parent", isOnEventListener: true }, called: false },
                    { listener: { id: "parent", capture: false }, called: true, order: 3 },
                    { listener: { id: "parent", isOnEventListener: true }, called: true, order: 1 },
                ],
            ],
        ])(
            "%s",
            // @ts-ignore: Arguments not match because Jest's `test.each` typings put all types in union type
            (
                description: string,
                eventToFire: { data: toMS.FireEventMsg },
                listeners: Array<{
                    listener: {
                        id: string;
                        capture: boolean;
                        isOnEventListener?: boolean;
                    };
                    order?: number;
                    called: boolean; // Checks wether it was called
                    listenerMock?: jest.MockedFunction<(e: Event) => void>;
                }>
            ) => {
                // A
                // Register all mocked listeners
                for (const listenerObject of listeners) {
                    const listener = listenerObject.listener;
                    if (listener.isOnEventListener) {
                        // @ts-ignore
                        listenerObject.listenerMock = document.getElementById(listener.id)!.onclick = jest.fn();
                    } else {
                        listenerObject.listenerMock = addMockHandler(listener.id, "click", listener.capture);
                    }
                }

                // A
                sendMessage(new MessageEvent("", eventToFire));

                // A
                let lastCallOrder = 0;
                listeners.sort((a, b) => (a.order ?? -1) - (b.order ?? -1));

                for (const listener of listeners) {
                    if (listener.called) {
                        expect(listener.listenerMock).toBeCalledTimes(1);
                    } else {
                        expect(listener.listenerMock).not.toBeCalled();
                        continue;
                    }

                    // Check order
                    expect(listener.listenerMock!.mock.invocationCallOrder[0]).toBeGreaterThanOrEqual(lastCallOrder);
                    lastCallOrder = listener.listenerMock!.mock.invocationCallOrder[0];
                }
            }
        );

        describe("Stopping propagation", () => {
            test.each([
                ["stopPropagation", (e: Event) => e.stopPropagation()],
                ["cancelBubble", (e: Event) => (e.cancelBubble = true)],
            ])("via %s", (description: string, stopping: (e: Event) => void) => {
                // A
                const first = jest.fn(stopping);
                document.getElementById("target")!.addEventListener("click", first);
                const second = addMockHandler("target", "click");
                const parent = addMockHandler("parent", "click");

                // A
                sendMessage(new MessageEvent("", GetFireEventMessage()));

                // A
                expect(first).toBeCalledTimes(1);
                expect(second).toBeCalledTimes(1);
                expect(parent).not.toBeCalled();
            });

            test("via stopImmediatePropagation", () => {
                // A
                const first = jest.fn((e: Event) => e.stopImmediatePropagation());
                document.getElementById("target")!.addEventListener("click", first);
                const second = addMockHandler("target", "click");
                const parent = addMockHandler("parent", "click");

                // A
                sendMessage(new MessageEvent("", GetFireEventMessage()));

                // A
                expect(first).toBeCalledTimes(1);
                expect(second).not.toBeCalled();
                expect(parent).not.toBeCalled();
            });
        });

        describe("Emitted events are indistinguishable from native events", () => {
            test("Capturing event has set phase to Capturing", () => {
                // A
                const mock = jest.fn();
                document.getElementById("parent")!.addEventListener("click", mock, true);

                // A
                sendMessage(new MessageEvent("", GetFireEventMessage()));
                sendMessage(new MessageEvent("", GetFireEventMessage({ capturingTargets: ["#parent"] })));

                // A
                expect(mock.mock.calls[0][0].eventPhase).toBe(Event.CAPTURING_PHASE);
                expect(mock.mock.calls[1][0].eventPhase).toBe(Event.CAPTURING_PHASE);
            });
            test("Event at target has set phase to atTarget", () => {
                // A
                const mock = jest.fn();
                document.getElementById("target")!.addEventListener("click", mock, false);
                document.getElementById("target")!.addEventListener("click", mock, true);

                // A
                sendMessage(new MessageEvent("", GetFireEventMessage()));
                sendMessage(new MessageEvent("", GetFireEventMessage({ atTarget: true })));

                // A
                expect(mock.mock.calls[0][0].eventPhase).toBe(Event.AT_TARGET);
                expect(mock.mock.calls[1][0].eventPhase).toBe(Event.AT_TARGET);
                expect(mock.mock.calls[2][0].eventPhase).toBe(Event.AT_TARGET);
                expect(mock.mock.calls[3][0].eventPhase).toBe(Event.AT_TARGET);
            });
            test("Bubbling event has set phase to bubbling", () => {
                // A
                const mock = jest.fn();
                document.getElementById("parent")!.addEventListener("click", mock);

                // A
                sendMessage(new MessageEvent("", GetFireEventMessage()));
                sendMessage(new MessageEvent("", GetFireEventMessage({ bubblingTargets: ["#parent"] })));

                // A
                expect(mock.mock.calls[0][0].eventPhase).toBe(Event.BUBBLING_PHASE);
                expect(mock.mock.calls[1][0].eventPhase).toBe(Event.BUBBLING_PHASE);
            });
            test.todo("Event not at target has specified properties");
            test("Event at target has specified properties", () => {
                // A
                const eventData: EventData = {
                    type: "click",
                    ctor: "MouseEvent",
                    targetQuery: "#target",
                    options: {
                        altKey: false,
                        button: 0,
                        buttons: 0,
                        clientX: 816,
                        clientY: 403,
                        ctrlKey: false,
                        layerX: 113,
                        layerY: 18,
                        metaKey: false,
                        movementX: 0,
                        movementY: 0,
                        offsetX: 70,
                        offsetY: 4,
                        pageX: 816,
                        pageY: 403,
                        screenX: 816,
                        screenY: 474,
                        shiftKey: false,
                        x: 816,
                        y: 403,
                        detail: 1,
                        bubbles: true,
                        cancelable: true,
                        composed: true,
                    },
                    bubblingTargets: ["#parent"],
                };

                // event's only own property
                const trusted = {
                    get isTrusted() {
                        // console.warn("blsasdfasdfah");
                        return true;
                    },
                };
                // important to get "get isTrusted" name for the getter function,
                // it is not possible to have function name with space otherwise.
                // !possibly chrome specific!
                Object.defineProperty(trusted, "isTrusted", {
                    enumerable: true,
                    configurable: false,
                });

                const mock = jest.fn();
                document.getElementById("parent")!.addEventListener("click", mock);

                // A
                sendMessage(new MessageEvent("", { data: { cmd: toMS.command.fireEvent, data: eventData } }));
                document.getElementById("target")!.dispatchEvent(new MouseEvent("click", eventData.options));

                // A
                expect(mock).toBeCalledTimes(2);

                const received = mock.mock.calls[0][0];
                expect(received).toPedanticEqual(trusted);

                expect(Object.getPrototypeOf(received)).toPedanticEqualInPrototype(
                    Object.getPrototypeOf(mock.mock.calls[1][0])
                );
            });
        });
    });

    describe("Recording events", () => {
        test.each([])("Correctly records %s events", (eventType: string, options: object, expected: object) => {
            // Arrange
            MessageChannelMock.port2.onmessage = jest.fn(event => {
                // Assert
                const msg = event.data as toCS.EventDataMsg;
                expect(msg.type).toMatchObject(expected);
            });

            addMockHandler("parent", eventType);
            sendMessage(
                new MessageEvent("", { data: { cmd: toMS.command.recEvents, events: [eventType] } as toMS.RecEventMsg })
            );

            // Act
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", options));

            // Assert
            expect(MessageChannelMock.port2.onmessage).toBeCalledTimes(1);
        });
        test("Records just specified events", () => {
            // Arrange
            MessageChannelMock.port2.onmessage = jest.fn(event => {
                // Assert
                const msg = event.data as toCS.EventDataMsg;
                expect(msg.msgType).toBe(toCS.msgType.eventData);
                expect(msg.type).toBe("click");
            });
            addMockHandler("parent", "click", true);
            sendMessage(
                new MessageEvent("", { data: { cmd: toMS.command.recEvents, events: ["click"] } as toMS.RecEventMsg })
            );

            // Act
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: false }));

            document.getElementById("target")!.dispatchEvent(new MouseEvent("mousemove", { bubbles: true }));
            document.getElementById("target")!.dispatchEvent(new KeyboardEvent("keydown", { bubbles: true }));

            // Assert
            expect(MessageChannelMock.port2.onmessage).toBeCalledTimes(2);
        });
        test("One event has one id", () => {
            // Arrange
            let eventId: undefined | number;
            MessageChannelMock.port2.onmessage = jest.fn(event => {
                // Assert
                const msg = event.data as toCS.EventDataMsg;
                expect(msg.msgType).toBe(toCS.msgType.eventData);
                if (eventId === undefined) eventId = msg.id;
                expect(msg.id).toBe(eventId);
            });
            addMockHandler("parent", "click", true);
            addMockHandler("target", "click", true);
            addMockHandler("target", "click", false);
            addMockHandler("parent", "click", false);

            sendMessage(
                new MessageEvent("", { data: { cmd: toMS.command.recEvents, events: ["click"] } as toMS.RecEventMsg })
            );

            // Act
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));

            // Assert
            expect(MessageChannelMock.port2.onmessage).toBeCalled();
        });
        test("After stop should no events be logged", () => {
            // Arrange
            MessageChannelMock.port2.onmessage = jest.fn();
            addMockHandler("parent", "click", true);
            sendMessage(
                new MessageEvent("", { data: { cmd: toMS.command.recEvents, events: ["click"] } as toMS.RecEventMsg })
            );
            // Act
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: true }));
            sendMessage(new MessageEvent("", { data: { cmd: toMS.command.stop } as toMS.Msg }));
            document.getElementById("target")!.dispatchEvent(new MouseEvent("click", { bubbles: false }));

            // Assert
            expect(MessageChannelMock.port2.onmessage).toBeCalledTimes(2);
        });
        test.todo("After stop, restart should?");
        test.todo("Support Custom event");
    });

    test.todo("window selector");
    test.todo("document selector");

    describe("Error handling ~ What sends error message", () => {
        test.each([
            ["Unknown command", { cmd: "fail" }, { error: InfoError.Unexpected } as ErrorMessageData],
            [
                "Missing `events` in `RecEventMsg`",
                { cmd: toMS.command.recEvents, events: [] } as toMS.RecEventMsg,
                { error: InfoError.Unexpected } as ErrorMessageData,
            ],
            [
                "`events` in `RecEventMsg` is empty array",
                { cmd: toMS.command.recEvents },
                { error: InfoError.Unexpected } as ErrorMessageData,
            ],
            [
                "Missing `type` in `FireEventMsg`",
                {
                    cmd: toMS.command.fireEvent,
                    data: { ctor: "MouseEvent", targetQuery: "#target" },
                } as toMS.FireEventMsg,
                { error: InfoError.Unexpected } as ErrorMessageData,
            ],
            [
                "Missing `type` in `ctor`",
                { cmd: toMS.command.fireEvent, data: { type: "click", targetQuery: "#target" } } as toMS.FireEventMsg,
                { error: InfoError.Unexpected } as ErrorMessageData,
            ],
            [
                "Missing `targetQuery` in `FireEventMsg`",
                { cmd: toMS.command.fireEvent, data: { type: "click", ctor: "MouseEvent" } } as toMS.FireEventMsg,
                { error: InfoError.Unexpected } as ErrorMessageData,
            ],
            [
                "Bad CSSSelector in `targetQuery`",
                {
                    cmd: toMS.command.fireEvent,
                    data: { type: ".fail", ctor: "MouseEvent", targetQuery: "#target" },
                } as toMS.FireEventMsg,
                { error: InfoError.Selector_NotFound } as ErrorMessageData,
            ],
            [
                "Bad CSSSelector in `bubblingTargets`",
                {
                    cmd: toMS.command.fireEvent,
                    data: { type: "click", ctor: "MouseEvent", targetQuery: "#target", bubblingTargets: [".fail"] },
                } as toMS.FireEventMsg,
                { error: InfoError.Selector_NotFound } as ErrorMessageData,
            ],
            [
                "Bad CSSSelector in `capturingTargets`",
                {
                    cmd: toMS.command.fireEvent,
                    data: { type: "click", ctor: "MouseEvent", targetQuery: "#target", capturingTargets: [".fail"] },
                } as toMS.FireEventMsg,
                { error: InfoError.Selector_NotFound } as ErrorMessageData,
            ],
            [
                "Unknown Event constructor",
                { cmd: toMS.command.fireEvent, data: ({ ctor: "fail" } as unknown) as EventData } as toMS.FireEventMsg,
                { error: InfoError.Unexpected } as ErrorMessageData, // todo
            ],
        ])(
            "%s",
            // @ts-ignore: Arguments not match because Jest's `test.each` typings put all types in union type
            (description: string, msg: toMS.Msg, error: ErrorMessageData) => {
                // Arrange
                MessageChannelMock.port2.onmessage = jest.fn(event => {
                    // Assert
                    const errorMsg = event.data as toCS.ErrorMsg;
                    expect(errorMsg.msgType).toBe(toCS.msgType.error);
                    expect(errorMsg.error.error).toBe(error);
                });

                // Act
                sendMessage(new MessageEvent("", { data: msg }));

                // Assert
                expect(MessageChannelMock.port2.onmessage).toBeCalled();
            }
        );
        // todo: add warning to custom events?
        // test.todo("Unknown event (eventData)");
    });
});
