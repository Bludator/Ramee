import "jest-extended";

let InjectOnceOnLoad: (tabID: number, reload: boolean) => Promise<any>;
let RemoveInjectOnLoad: (tabID: number) => void;

beforeEach(async () => {
    // imports before each test, prevents side-effects
    ({ InjectOnceOnLoad, RemoveInjectOnLoad } = await import("../../src/InPageScript/insert"));

    // set up additional mocks
    chrome.runtime.id = "extensionID";
    chrome.runtime.getURL = jest.fn((path: string) => `${chrome.runtime.id}/url/of/${path}`);
    chrome.tabs.get = jest.fn((tabId: number, callback: (tab: chrome.tabs.Tab) => void) =>
        callback({
            index: 0,
            url: `url.of/tab/${tabId}`,
            pinned: false,
            highlighted: false,
            windowId: 1,
            active: true,
            id: tabId,
            incognito: false,
            selected: true,
            discarded: false,
            autoDiscardable: true,
        })
    );
    chrome.tabs.executeScript = jest.fn(
        (
            tabId: number | chrome.tabs.InjectDetails,
            details?: chrome.tabs.InjectDetails | ((result: any[]) => void),
            callback?: (result: any[]) => void
        ) => {
            if (callback) callback([]);
        }
    );
    chrome.tabs.reload = jest.fn();
    chrome.webRequest = {
        onCompleted: {
            addListener: jest.fn((
                callback: (details: chrome.webRequest.WebResponseCacheDetails) => void,
                filter?: chrome.webRequest.RequestFilter | undefined,
                // tslint:disable-next-line: variable-name
                opt_extraInfoSpec?: string[] | undefined
            ) => callback({} as any)),
            removeListener: jest.fn(),
        } as any,
    } as any;

    // No fetch mock in Jest :-(
    window.fetch = (jest.fn(() =>
        Promise.resolve({ text: () => "document.assignedFromInsideOfInPageScript = 'InPageScript'" })
    ) as unknown) as typeof window.fetch;
});

describe("InjectOnceOnLoad(tabID, reload) ~ AddListener to reload/redirect to inject inPageScript in page", () => {
    test("inserts on redirect of specified tab", async () => {
        // A
        const tabId = 2;

        // A
        await InjectOnceOnLoad(1, false);
        await InjectOnceOnLoad(tabId, false);
        await InjectOnceOnLoad(3, false);

        // A
        // Check if it is set filter
        expect(chrome.webRequest.onCompleted.addListener).toBeCalledWith(
            expect.anything(),
            expect.objectContaining({ tabId })
        );
        expect(chrome.tabs.executeScript).toBeCalledTimes(1);
        expect(chrome.tabs.executeScript).toBeCalledWith(tabId, expect.anything(), expect.anything());
    });

    test("inserts only once", async () => {
        // A
        // A
        await InjectOnceOnLoad(1, false);

        // A
        expect(chrome.webRequest.onCompleted.removeListener).toBeCalledTimes(1);
        expect(chrome.webRequest.onCompleted.removeListener).toBeCalledWith(
            (chrome.webRequest.onCompleted.addListener as jest.Mock).mock.calls[0][0]
        );
    });
    test("if `reload` = true, reloads tab", async () => {
        // A
        // A
        await InjectOnceOnLoad(1, true);

        // A
        expect(chrome.tabs.reload).toBeCalled();
    });

    test("inserts `InPageScript.ts`", async () => {
        // A
        // A
        await InjectOnceOnLoad(1, false);
        const code: string = (chrome.tabs.executeScript as jest.Mock).mock.calls[0][1].code;
        Function(code)();

        // A
        const path: string = (window.fetch as jest.Mock).mock.calls[0][0];
        expect(path).toStartWith(chrome.runtime.id);
        expect(path).toEndWith("InPageScript/index.js");
        expect((document as any).assignedFromInsideOfInPageScript).toBe("InPageScript");
    });
    test("doesn't change html in production mode", async () => {
        // A
        process.env.NODE_ENV = NODE_ENV.production;

        // A
        await InjectOnceOnLoad(1, false);
        const code = (chrome.tabs.executeScript as jest.Mock).mock.calls[0][1].code;
        Function(code)();

        // A
        expect(document.head.innerHTML).toBeEmpty();
        expect(document.body.innerHTML).toBeEmpty();
        expect(document.documentElement.innerHTML).toBe("<head></head><body></body>");
    });
    /*test("registers global variable", async () => {
        // A
        // A
        await InjectOnceOnLoad(1, false);
        const code: string = (chrome.tabs.executeScript as jest.Mock).mock.calls[0][1].code;
        Function(code)();

        // A
        const path: string = (window.fetch as jest.Mock).mock.calls[0][0];
        expect(path).toStartWith(chrome.runtime.id);
        expect(path).toEndWith("InPageScript/index.js");
        expect((window as any).InPage).toMatchObject({ script: expect.anything(), port: expect.anything() });
    });*/
    test("undefined tab's URL (e.g. in devtools)", async () => {
        // A
        chrome.tabs.get = jest.fn((tabId: number, callback: (tab: chrome.tabs.Tab) => void) =>
            callback({
                index: 0,
                url: undefined,
                pinned: false,
                highlighted: false,
                windowId: 1,
                active: true,
                id: tabId,
                incognito: false,
                selected: true,
                discarded: false,
                autoDiscardable: true,
            })
        );

        // A
        // A
        expect(InjectOnceOnLoad(69, false)).toReject(); // todo
    });
    test("multiple listeners with the same tabID returns the same promise", async () => {
        // A
        // A
        const tabTwo = InjectOnceOnLoad(2, false);
        const tabTwo2 = InjectOnceOnLoad(2, false);

        // A
        expect(tabTwo).toBe(tabTwo2);
    });
});

describe("RemoveListener(tabID) ~ removes listeners for tab (added via InjectOnceOnLoad)", () => {
    test("removes listener for the tab", async () => {
        // A
        // A
        await InjectOnceOnLoad(1, false);
        RemoveInjectOnLoad(1);

        // A
        expect(chrome.webRequest.onCompleted.removeListener).toBeCalledTimes(1);
        expect(chrome.webRequest.onCompleted.removeListener).toBeCalledWith(
            (chrome.webRequest.onCompleted.addListener as jest.Mock).mock.calls[0][0]
        );
    });
    test("removes all listeners for the same tab", async () => {
        // Not really testable ... it tests implementation details
        // A
        // A
        await InjectOnceOnLoad(1, false);
        await InjectOnceOnLoad(2, false);
        await InjectOnceOnLoad(2, false);
        await InjectOnceOnLoad(3, false);

        RemoveInjectOnLoad(2);

        // A
        expect((chrome.webRequest.onCompleted.removeListener as jest.Mock).mock.calls.length).toBeLessThanOrEqual(2);
        expect(chrome.webRequest.onCompleted.removeListener).toBeCalledWith(
            (chrome.webRequest.onCompleted.addListener as jest.Mock).mock.calls[1][0]
        );
    });
});
