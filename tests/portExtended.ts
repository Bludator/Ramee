describe("Port Extension (adds connected:bool property)", () => {
    beforeEach(() => {
        // @ts-ignore import
        import("../src/_common/portExtended");
    });
    test("chrome.runtime.Port", () => {
        const port = chrome.runtime.connect({ name: "test" });
        expect(port.connected).toBe(true);
        port.disconnect();
        expect(port.connected).toBe(false);
    });
    test("chrome.tabs.Port", () => {
        const port = chrome.tabs.connect(10, { name: "test" });
        expect(port.connected).toBe(true);
        port.disconnect();
        expect(port.connected).toBe(false);
    });
});
