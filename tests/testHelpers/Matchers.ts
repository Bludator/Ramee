// tslint:disable-next-line: no-require-imports
const diff = require("jest-diff");
declare module "" {
    global {
        namespace jest {
            interface Matchers<R> {
                toEqualInPrototype(expected: object): R;
                toPedanticEqualInPrototype(expected: object): R;
                // toDeepPedanticEqualInPrototype(expected: object): R;

                /**
                 * Deep equal with pedantic shallow equal
                 * @param expected
                 */
                toPedanticEqual(expected: object): R;
                // toDeepPedanticEqual(expected: object): R;
            }
        }
    }
}
expect.extend({ toPedanticEqual });

expect.extend({
    toEqualInPrototype(received, expected) {
        return prototypeEqual.call(this, received, expected, equalsWrapper);
    },
});
expect.extend({
    toPedanticEqualInPrototype(received, expected) {
        return prototypeEqual.call(this, received, expected, toPedanticEqual);
    },
});

function stringifyDescriptors(x: any): string {
    if (typeof x !== "object") {
        return x.toString();
    }
    return JSON.stringify(
        Object.getOwnPropertyDescriptors(x),
        (key: string, value: any) => {
            if (typeof value === "function") {
                const functionObj = { ".function": { name: value.name } };
                if (key === "get") {
                    let expandedValue;
                    try {
                        expandedValue = value();
                        functionObj.value = stringifyDescriptors(expandedValue);
                    } catch (error) {
                        if (error.message !== "Illegal invocation") {
                            throw error;
                        }
                    }
                }
                return functionObj;
            } else return value;
        },
        2
    );
}

function toPedanticEqual(
    this: jest.MatcherContext,
    received: any,
    expected: any
): { pass: boolean; message: () => string } {
    const parsedExpected = stringifyDescriptors(expected);
    const parsedReceived = stringifyDescriptors(received);
    const options = {
        comment: "object's property descriptors equality",
        isNot: this.isNot,
        promise: this.promise,
    };
    const pass = parsedReceived === parsedExpected;

    const message = pass
        ? () =>
              this.utils.matcherHint("toPedanticEqual", undefined, undefined, options) +
              "\n\n" +
              `Expected: not ${this.utils.printExpected(parsedExpected)}\n` +
              `Received: ${this.utils.printReceived(parsedReceived)}`
        : () => {
              const diffString = diff(parsedExpected, parsedReceived, {
                  expand: this.expand,
              });
              return (
                  this.utils.matcherHint("toPedanticEqual", undefined, undefined, options) +
                  "\n\n" +
                  (diffString && diffString.includes("- Expect")
                      ? `Difference:\n\n${diffString}`
                      : `Expected: ${this.utils.printExpected(parsedExpected)}\n` +
                        `Received: ${this.utils.printReceived(parsedReceived)}`)
              );
          };
    return { message, pass };
}

function equalsWrapper(
    this: jest.MatcherContext,
    received: any,
    expected: any
): { pass: boolean; message: () => string } {
    const options = {
        comment: "Object.is equality",
        isNot: this.isNot,
        promise: this.promise,
    };
    try {
        const pass = this.equals(received, expected);
        const message = pass
            ? () =>
                  this.utils.matcherHint("toEqual", undefined, undefined, options) +
                  "\n\n" +
                  `Expected: not ${this.utils.printExpected(expected)}\n` +
                  `Received: ${this.utils.printReceived(received)}`
            : () => {
                  const diffString = diff(expected, received, {
                      expand: this.expand,
                  });
                  return (
                      this.utils.matcherHint("toEqual", undefined, undefined, options) +
                      "\n\n" +
                      (diffString && diffString.includes("- Expect")
                          ? `Difference:\n\n${diffString}`
                          : `Expected: ${this.utils.printExpected(expected)}\n` +
                            `Received: ${this.utils.printReceived(received)}`)
                  );
              };
        return { message, pass };
    } catch (error) {
        if (error.message === "Illegal invocation") {
            throw error; // todo some recovery
        }
        throw error;
    }
}

function prototypeEqual(
    this: jest.MatcherContext,
    received: object,
    expected: object,
    comparer: (
        this: jest.MatcherContext,
        received: object,
        expected: object
    ) => { pass: boolean; message: () => string }
) {
    const originalExpected = expected;
    const originalReceived = received;
    do {
        const result = comparer.call(this, received, expected);
        if (!result.pass && !this.isNot) {
            return result;
        }

        expected = Object.getPrototypeOf(expected);
        received = Object.getPrototypeOf(received);
    } while (expected !== null || received !== null);
    if (expected === received) {
        return {
            pass: true,
            message: () => `expected ${originalReceived} not to equal in prototype ${originalExpected}`,
        };
    } else {
        return {
            pass: false,
            message: () => `expected to have prototype${expected}; received ${received}`,
        };
    }
}

// to print in console
/*
function printProto(obj) {
    const array = [];
    do {
        array.push(obj);
        obj = Object.getPrototypeOf(obj);
    } while (obj);
    let str = basd(array[array.length - 1]);
    for (let i = array.length - 2; i >= 0; i--) {
        const prepend = basd(array[i]).slice(0, -1);
        str = prepend + (prepend.length < 10 ? "" : ",") + `"__proto__": ${str}}`;
    }

    return str;
}*/
